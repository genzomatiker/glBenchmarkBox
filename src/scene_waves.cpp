/*******************************************************************************
* file   : scene_waves.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "scene_waves.h"

// =============================================================================
// =               waves geometric details                                    =
// =============================================================================
namespace waves
{
    Vertex3D<float> quadVert[] = { {  0,  0, 0 },       //  [0] left  up
                                   {  1,  0, 0 },       //  [1] left  bottom
                                   {  1,  1, 0 },       //  [2] right bottom
                                   {  0,  1, 0 } };     //  [3] right up

    unsigned        quadIdx[]  =   {  0, 1, 2,              //  [0] triangle 1
                                      2, 3, 0  };           //  [1] triangle 2

    unsigned        quadIdxCnt = 6;
}

using namespace std;
using namespace waves;


// =============================================================================
// =               class members                                               =
// =============================================================================
SceneWaves::SceneWaves ( float w, float h, unsigned triCnt, GLenum format ) : ScenePrimitives()
{
    m_size    = Vertex3D<float>( w, h, 0 );
    m_triCnt  = triCnt;
    m_waveSrc = Vertex3D<float>( 0, 0, -1 );
    m_waveMax = 0.5;
    m_waveCnt = 5;
    init( format );
}

bool SceneWaves::init ( GLenum meshFormat )
{
    // compute dimensions
    unsigned qWish      = m_triCnt / 2;
    unsigned xQcnt      = lrintf( sqrtf( qWish ) );
    unsigned yQcnt      = qWish / xQcnt;
    unsigned qCnt       = xQcnt * yQcnt;
    Vertex3D<float> qSz = Vertex3D<float>( m_size.x / xQcnt, m_size.y / yQcnt, 1 );

    // allocate RAM for mesh (can be huge!)
    m_triCnt = qCnt * 2;
    bool allocOK = ScenePrimitives::init( m_triCnt * 3, GL_TRIANGLES, meshFormat );
    if ( allocOK )
    {
        unsigned  idx = 0;
        for ( Vertex3D<float> q; q.y < yQcnt; ++q.y )
            for ( q.x = 0; q.x < xQcnt; ++q.x )
                for ( unsigned qIdx = 0; qIdx < quadIdxCnt; ++qIdx, ++idx )
                {
                    // compute x/y dimensions of current vertex
                    Vertex3D<float> vert = quadVert[quadIdx[qIdx]]*qSz - m_size/2 + q*qSz;

                    // compute z dimension of current vertex
                    float waveRadius = (m_waveSrc - vert).length();
                    float waveAng    = (360 * m_waveCnt * waveRadius / m_size.length()) * float(M_PI) / float(180);
                    vert.z           = vert.z + sinf( waveAng ) * m_waveMax - m_waveMax;

                    switch ( m_meshFormat )
                    {
                        case GL_C4F_N3F_V3F:
                            color( idx )[3] = 1;
                        case GL_C3F_V3F:
                            color( idx )[0] = q.x / xQcnt;
                            color( idx )[1] = 0.9;
                            color( idx )[2] = 1 - 0.9 * q.x / xQcnt;
                        case GL_V3F:
                            vertex( idx ) = vert;
                            break;
                    }
                    p_indexes[idx] = idx;
                }

        // compute normals after vertexes are known
        if ( GL_N3F_V3F == m_meshFormat || GL_C4F_N3F_V3F == m_meshFormat )
            init_normals_flat();
    }

    return allocOK;
}

#include <iostream>
void SceneWaves::init_normals_smooth ()
{
    cerr << "error: init_normals_smooth() not implemented yet!" << endl;
    exit( EXIT_FAILURE );
}

void SceneWaves::init_normals_flat ()
{
    for ( unsigned idx = 0; idx < element_count(); idx += 3 )
    {
        Vertex3D<float> n = (vertex(idx+1) - vertex(idx)) % (vertex(idx+2) - vertex(idx));
        normal( idx   ) = n.normalize();
        normal( idx+1 ) = n.normalize();
        normal( idx+2 ) = n.normalize();
    }
}
