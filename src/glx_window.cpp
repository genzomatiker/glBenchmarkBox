/*******************************************************************************
* file   : glx_window.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "glx_window.h"
#include "glx_attr_list.h"
#include <iostream>

using namespace std;

// =============================================================================
// = global variables
// =============================================================================
// NOTE: It is not necessary to create or make current to a context before calling glXGetProcAddressARB
typedef GLXContext (*glxCreateContAttrArbFunc) ( Display*, GLXFBConfig, GLXContext, Bool, const int* );
glxCreateContAttrArbFunc glxCreateContAttrARB = (glxCreateContAttrArbFunc) glXGetProcAddressARB( (const GLubyte*) "glXCreateContextAttribsARB" );

// =============================================================================
// = local macros
// =============================================================================
#define RESET_MEMBERS( pTitle, x, y, w, h )                                    \
{                                                                              \
    m_window   = 0;                                                            \
    p_title    = pTitle;                                                       \
    m_position.set( x, y );                                                    \
    m_geometry.set( w, h );                                                    \
    p_fbConfig = nullptr;                                                      \
    p_context  = nullptr;                                                      \
}

#define HANDLE_WINDOW_CREATION_ERROR                                           \
    if ( !m_window )                                                           \
    {                                                                          \
        cerr << "error: unable to create window" << endl;                      \
        ++errors();                                                            \
    }

#define INIT_WINDOW                                                            \
    if ( m_window )                                                            \
    {                                                                          \
        show( false );                                                         \
        title( p_title );                                                      \
    }

// =============================================================================
// = member functions
// =============================================================================
// create invisible window
GlxWindow::GlxWindow ( GlxDisplay &dspl, int x, int y, unsigned w, unsigned h ) : ErrorCounter( m_xErrCnt ),
                                                                                  m_display( dspl )
{
    RESET_MEMBERS( "<unnamed window>", x, y, w, h )
    m_window = XCreateSimpleWindow( m_display.display(), m_display.root_win(), x, y, w, h, 2, m_display.white_pixel(), m_display.black_pixel() );
    HANDLE_WINDOW_CREATION_ERROR
    INIT_WINDOW
    // CAUTION: no frame buffer config used here => p_fbConfig is NULL
}

// create invisible window
GlxWindow::GlxWindow ( GlxDisplay &dspl, int x, int y, unsigned w, unsigned h, XVisualInfo &visualInfo ) : ErrorCounter( m_xErrCnt ),
                                                                                                           m_display( dspl )
{
    RESET_MEMBERS( "<unnamed window>", x, y, w, h )
    Window               rootWin = RootWindow( m_display.display(), visualInfo.screen );
    XSetWindowAttributes swa;
    swa.colormap                 = XCreateColormap( m_display.display(), rootWin, visualInfo.visual, AllocNone );
    swa.background_pixmap        = None;
    swa.border_pixel             = 0;
    swa.event_mask               = StructureNotifyMask;
    m_window = XCreateWindow( m_display.display(), rootWin, m_position.x, m_position.y, m_geometry.x, m_geometry.y, 0,
                              visualInfo.depth, InputOutput, visualInfo.visual,
                              CWBorderPixel | CWColormap | CWEventMask, &swa );
    HANDLE_WINDOW_CREATION_ERROR
    INIT_WINDOW
    // CAUTION: no frame buffer config used here => p_fbConfig is NULL
}

// create invisible window
GlxWindow::GlxWindow ( GlxDisplay &dspl, int x, int y, unsigned w, unsigned h, XVisualInfo &visualInfo, GLXFBConfig pFbConfig ) : ErrorCounter( m_xErrCnt ),
                                                                                                                                  m_display( dspl )
{
    RESET_MEMBERS( "<unnamed window>", x, y, w, h )
    Window               rootWin = RootWindow( m_display.display(), visualInfo.screen );
    XSetWindowAttributes swa;
    swa.colormap                 = XCreateColormap( m_display.display(), rootWin, visualInfo.visual, AllocNone );
    swa.background_pixmap        = None;
    swa.border_pixel             = 0;
    swa.event_mask               = StructureNotifyMask;
    m_window = XCreateWindow( m_display.display(), rootWin, m_position.x, m_position.y, m_geometry.x, m_geometry.y, 0,
                              visualInfo.depth, InputOutput, visualInfo.visual,
                              CWBorderPixel | CWColormap | CWEventMask, &swa );
    HANDLE_WINDOW_CREATION_ERROR
    INIT_WINDOW
    p_fbConfig = pFbConfig;
    create_glx_context();
}

GlxWindow::~GlxWindow ()
{
    if ( m_window )
    {
        show( false );
        XDestroyWindow( m_display.display(), m_window );
    }

    if ( p_context )
        glXDestroyContext( display().display(), p_context );

    RESET_MEMBERS( nullptr, 0, 0, 0, 0 )
}

GlxWindow& GlxWindow::create_glx_context ()
{
    // create context depending on the GLX version
    bool arbContext     = m_display.supported_function( "GLX_ARB_create_context" ) && glxCreateContAttrARB;
    bool arbContext_3_0 = arbContext                    && create_glx_context( 3, 0 );
    bool arbContext_1_0 = arbContext && !arbContext_3_0 && create_glx_context( 1, 0 );
    bool glxContext_1_3 = false;
    if ( !arbContext_3_0 && !arbContext_1_0 )
    {
        p_context      = glXCreateNewContext( display().display(), p_fbConfig, GLX_RGBA_TYPE, 0, True );
        glxContext_1_3 = !p_context;
    }

    if ( arbContext_3_0 || arbContext_1_0 || glxContext_1_3 )
    {
        bool directRendering = glXIsDirect( display().display(), p_context );
        if ( !directRendering )
            cout << "warn: Indirect GLX rendering context obtained" << endl;

        glXMakeCurrent( display().display(), m_window, p_context );
    }
    else
    {
        cerr << "error: failed to create GLX context" << endl;
        ++errors();
    }

    if ( arbContext_3_0 )
        cout << "info: created GLX context v.3.0" << endl;
    if ( arbContext_1_0 )
        cout << "info: created GLX context v.1.0" << endl;
    if ( glxContext_1_3 )
        cout << "info: created GLX context v.1.3" << endl;

    return *this;
}

GLXContext GlxWindow::create_glx_context ( int glxMajVer, int glxMinVer )
{
    GlxAttrList attrList;
    attrList.add( GLX_CONTEXT_MAJOR_VERSION_ARB, glxMajVer );
    attrList.add( GLX_CONTEXT_MINOR_VERSION_ARB, glxMinVer );
    //attrList.add( GLX_CONTEXT_FLAGS_ARB,         0 );
    //attrList.add( GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, 0 );

    int *pAttrArray = attrList.create_int_list();
    p_context = glxCreateContAttrARB( display().display(), p_fbConfig, 0, True, pAttrArray );
    delete []pAttrArray;

    display().sync();
    return !errors() && p_context ? p_context : nullptr;
}

// make window visible
GlxWindow& GlxWindow::show( bool visible )
{
    if ( visible )
        XMapWindow( m_display.display(), m_window );
    else
        XUnmapWindow( m_display.display(), m_window );

    m_display.flush();
    return *this;
}

GlxWindow& GlxWindow::title ( const char *pTitle )
{
    XStoreName( m_display.display(), m_window, pTitle );
    return *this;
}

GlxWindow& GlxWindow::swap_buffers ()
{
    glXSwapBuffers( display().display(), m_window );
    return *this;
}

GlxWindow& GlxWindow::clear ()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    return *this;
}

GlxDisplay& GlxWindow::display ()
{
    return m_display;
}

Window GlxWindow::window ()
{
    return m_window;
}

const Vertex2D<int>& GlxWindow::geometry () const
{
    return m_geometry;
}
const Vertex2D<int>& GlxWindow::position () const
{
    return m_position;
}
