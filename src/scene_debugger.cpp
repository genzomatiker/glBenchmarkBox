/*******************************************************************************
* file   : scene_debugger.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "scene_debugger.h"
#include <GL/gl.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include "scene_icosphere.h"
#include "tracing.h" // <---- only for trace/debug purposes -> remove

using namespace std;


// =============================================================================
// =               utils                                                       =
// =============================================================================
unsigned face_edge_count ( GLenum mode )
{
    switch ( mode )
    {
        case GL_POINTS:
            return 1;
        case GL_LINES:
            return 2;
        case GL_TRIANGLES:
            return 3;
        case GL_QUADS:
            return 4;
        case GL_POLYGON:   // can't say
        case GL_LINE_LOOP:
        case GL_LINE_STRIP:
        case GL_TRIANGLE_STRIP:
        case GL_TRIANGLE_FAN:
        case GL_QUAD_STRIP:
        default :;
    }

    return 0;
}

// =============================================================================
// =               class members                                               =
// =============================================================================
SceneDebugger::SceneDebugger ( const ScenePrimitives &primitives ) : ScenePrimitives()
{
    init( primitives );
}

// assign members
bool SceneDebugger::init ( const ScenePrimitives &primitives )
{
    // compute normal lines from given primitive entity
    ScenePrimitives         &mesh    = *(ScenePrimitives*)&primitives;
    unsigned                 edgeCnt = face_edge_count( primitives.mode() );
    vector<Vertex3D<float> > normalLines;
    for ( unsigned idx = 0; idx < mesh.element_count(); idx += edgeCnt )
    {
        bool flatFace = mesh.normal(idx) == mesh.normal(idx+1) && mesh.normal(idx) == mesh.normal(idx+1);
        if ( flatFace )
        {
            Vertex3D<float> faceCenter;
            for ( unsigned vIdx = 0; vIdx < edgeCnt; ++vIdx )
                faceCenter = faceCenter + mesh.vertex(idx + vIdx);
            faceCenter = faceCenter / edgeCnt;
            normalLines.push_back( faceCenter );
            normalLines.push_back( faceCenter + mesh.normal(idx) );
        }
        else
            for ( unsigned vIdx = 0; vIdx < edgeCnt; ++vIdx )
            {
                normalLines.push_back( mesh.vertex( idx + vIdx ) );
                normalLines.push_back( mesh.vertex( idx + vIdx ) + mesh.normal( idx + vIdx ) );
            }
    }

    // =========================================================================
    // instantiate members
    unsigned elemCnt = normalLines.size();
    bool allocOK     = ScenePrimitives::init( elemCnt, GL_LINES, GL_C3F_V3F );
    if ( allocOK )
        for ( unsigned idx = 0; idx < element_count(); ++idx )
        {
            color( idx ).set( 1, 1, 0 );
            vertex( idx ) = normalLines[idx];
            index( idx )  = idx;
        }

    return allocOK;
}
