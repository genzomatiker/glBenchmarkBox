/*******************************************************************************
* file   : scene_cube.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef scene_cube_H
#define scene_cube_H

#include "scene_primitives.h"


struct SceneCube : public ScenePrimitives
{
                            SceneCube ();
    bool                    init ();
};

#endif   // scene_cube_H
