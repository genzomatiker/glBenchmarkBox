/*******************************************************************************
* file   : vertex_2d.cpp.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "vertex_2d.h"


template<typename Type>
Vertex2D<Type>::Vertex2D ( Type defVal )
{
    set( defVal, defVal );
}

template<typename Type>
Vertex2D<Type>::Vertex2D ( Type X, Type Y )
{
    set( X, Y );
}

template<typename Type>
Vertex2D<Type>::Vertex2D ( const Vertex2D<Type> &cpy )
{
    *this = cpy;
}

template<typename Type>
Vertex2D<Type>& Vertex2D<Type>::operator = ( const Vertex2D<Type> &cpy )
{
    x = cpy.x;
    y = cpy.y;
    return *this;
}

template<typename Type>
Type& Vertex2D<Type>::operator [] ( unsigned idx )
{
    Type *pArray = &x;
    return pArray[idx];
}

template<typename Type>
Vertex2D<Type>& Vertex2D<Type>::set ( Type X, Type Y )
{
    x = X;
    y = Y;
    return *this;
}

template<typename Type>
Vertex2D<Type> Vertex2D<Type>::operator + ( const Vertex2D<Type> &sub ) const
{
    return Vertex2D<Type>( x + sub.x, y + sub.y );
}

template<typename Type>
Vertex2D<Type> Vertex2D<Type>::operator - ( const Vertex2D<Type> &sub ) const
{
    return Vertex2D<Type>( x - sub.x, y - sub.y );
}
