/*******************************************************************************
* file   : scene_cube.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "scene_cube.h"
#include <GL/gl.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include "tracing.h" // <---- only for trace/debug purposes -> remove

// =============================================================================
// =               cubes geometric details                                     =
// =============================================================================
namespace cube
{
    Vertex3D<float> pVertices[] = { { -1,  1,  1 },     //  [0] left  up     front
                                    { -1, -1,  1 },     //  [1] left  bottom front
                                    {  1,  1,  1 },     //  [2] right up     front
                                    {  1, -1,  1 },     //  [3] right bottom front
                                    {  1,  1, -1 },     //  [4] right up     back
                                    {  1, -1, -1 },     //  [5] right bottom back
                                    { -1,  1, -1 },     //  [6] left  up     back
                                    { -1, -1, -1 } };   //  [7] left  bottom back

    unsigned        pIndexes[] =    {  0, 1, 3, 2,      //  [0] front-1
                                       2, 3, 5, 4,      //  [1] front-2
                                       4, 5, 7, 6,      //  [2] right-1
                                       6, 7, 1, 0,      //  [3] right-2
                                       0, 2, 4, 6,      //  [4] back-1
                                       1, 7, 5, 3 };    //  [5] back-1}

    Vertex3D<float> pColors[] =   { {  1,  0,  0 },     //  [0] left  up     front
                                    { .7, .4,  0 },     //  [1] right up     front
                                    { .4, .7,  0 },     //  [2] right bottom front
                                    { .7, .4,  0 },     //  [3] left  bottom front
                                    { .7, .4,  0 },     //  [4] right up     back
                                    { .4, .7,  0 },     //  [5] left  up     back
                                    { .1,  1,  0 },     //  [6] right bottom back
                                    { .4, .7,  0 } };   //  [7] left  bottom back

    unsigned        faceEdgeCnt = 4;
}

using namespace cube;


// =============================================================================
// =               utils                                                       =
// =============================================================================
Vertex3D<float> plane_normal ( unsigned planeIdx )
{
    unsigned        nI    = planeIdx * 4;
    Vertex3D<float> tV[3] = { pVertices[pIndexes[nI]], pVertices[pIndexes[nI+1]], pVertices[pIndexes[nI+3]] };
    Vertex3D<float> lightVector = (tV[1]-tV[0]) % (tV[2]-tV[0]);
    return lightVector.normalize();
}

// =============================================================================
// =               class members                                               =
// =============================================================================
SceneCube::SceneCube() : ScenePrimitives()
{
    init();
}

// assign vertices, colors, normals and indexes
bool SceneCube::init ()
{
    // instantiate members
    unsigned elemCnt = sizeof( pIndexes ) / sizeof( unsigned );
    bool allocOK     = ScenePrimitives::init( elemCnt, GL_QUADS, GL_C4F_N3F_V3F );
    if ( allocOK )
        for ( unsigned idx = 0; idx < m_indexCnt; ++idx )
        {
            color( idx )[0] = pVertices[ pIndexes[idx] ].x;
            color( idx )[1] = pVertices[ pIndexes[idx] ].y;
            color( idx )[2] = pVertices[ pIndexes[idx] ].z;
            color( idx )[3] = 1;
            normal( idx )   = plane_normal( idx / faceEdgeCnt );
            vertex( idx )   = pVertices[ pIndexes[idx] ];
            index( idx )    = idx;
        }

    return allocOK;
}
