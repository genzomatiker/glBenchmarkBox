/*******************************************************************************
* file   : vertex_3d.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef vertex_3d_H
#define vertex_3d_H


// offers an easy interface to Vertex array members and make indexing unnecessary
template<typename Type>
struct Vertex3D
{
public:
                                Vertex3D ( Type defVal = 0 );
                                Vertex3D ( Type x, Type y, Type z );
                                Vertex3D ( const Vertex3D<Type> &cpy );
    Vertex3D<Type>&             operator = ( const Vertex3D<Type> &cpy );
    bool                        operator == ( const Vertex3D<Type> &cmp );
    Type&                       operator [] ( unsigned idx );
    Vertex3D<Type>&             set ( Type x, Type y, Type z );
    // -------------------------------------------------------------------------
    Vertex3D<Type>              operator + ( const Vertex3D<Type> &add ) const;
    Vertex3D<Type>              operator - ( const Vertex3D<Type> &sub ) const;
    Vertex3D<Type>              operator * ( const Vertex3D<Type> &mul ) const;
    Vertex3D<Type>              operator / ( const Vertex3D<Type> &div ) const;
    Vertex3D<Type>              operator % ( const Vertex3D<Type> &mul ) const;
    // -------------------------------------------------------------------------
    Vertex3D<Type>              operator + ( const Type           &add ) const;
    Vertex3D<Type>              operator - ( const Type           &sub ) const;
    Vertex3D<Type>              operator * ( const Type           &mul ) const;
    Vertex3D<Type>              operator / ( const Type           &div ) const;
    // -------------------------------------------------------------------------
    Vertex3D<Type>              rotate_x ( float degrees );
    Vertex3D<Type>              rotate_y ( float degrees );
    Vertex3D<Type>              rotate_z ( float degrees );
    // -------------------------------------------------------------------------
    Type                        length ();
    Vertex3D<Type>              normalize ( float len = 1 );

public:
    Type                            x;
    Type                            y;
    Type                            z;
};

// template implementation must be included by declaring header file
#include "vertex_3d.cpp.h"

#endif   // vertex_3d_H
