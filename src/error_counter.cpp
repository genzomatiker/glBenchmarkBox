/*******************************************************************************
* file   : error_counter.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "error_counter.h"


ErrorCounter::ErrorCounter ( unsigned &errCnt ) : m_errorCnt( errCnt )
{
    m_errorCnt = 0;
}

ErrorCounter::operator bool () const
{
    return !m_errorCnt;
}

unsigned& ErrorCounter::errors ()
{
    return m_errorCnt;
}
