/*******************************************************************************
* file   : glx_input.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "glx_input.h"
#include <string.h>
#include <iostream>
#include <X11/XKBlib.h>

using namespace std;


GlxInput::GlxInput ( GlxWindow &win, long eventMask ) : ErrorCounter( m_xErrCnt ),
                                                        m_window( win )
{
    m_xErrCnt        = 0;
    m_keyEventCnt    = 0;
    memset( m_key, 0, sizeof(m_key) );
    m_btnEventCnt    = 0;
    memset( m_btn, 0, sizeof(m_btn) );
    m_cursorEventCnt = 0;

    // only handle the events we're interested in
    XSelectInput( m_window.display().display(), m_window.window(), eventMask );

    // turn off keyboard's auto repeat to not waste CPU time (marginal benefit)
    // ATTENTION: when turning off auto repeat the turning on has to appear in a crash handler!
//    XAutoRepeatOff( m_window.display().display() );
}

GlxInput::~GlxInput ()
{
    XAutoRepeatOn( m_window.display().display() );
}

unsigned GlxInput::poll_events ()
{
    unsigned events = XPending( m_window.display().display() );
    for ( unsigned unhandled = events; unhandled; --unhandled )
        read_event();

    return events;
}

// decode and store event
GlxInput& GlxInput::read_event ()
{
    XEvent event;
    XNextEvent( m_window.display().display(), &event );
    switch ( event.type )
    {
        case KeyPress:
        {
            KeySym keySym = XLookupKeysym( &event.xkey, 0 );
            m_key[keySym & 255] = 1;
            ++m_keyEventCnt;
            //cout << "key press: " << (keySym & 255) << endl;
            break;
        }
        case KeyRelease:
        {
            KeySym keySym = XLookupKeysym( &event.xkey, 0 );
            m_key[keySym & 255] = 0;
            ++m_keyEventCnt;
            //cout << "key release: " << (keySym & 255) << endl;
            break;
        }

        case ButtonPress:
        {
            m_btn[event.xbutton.button - 1] = 1;
            ++m_btnEventCnt;
            //cout << "mouse button press: " << event.xbutton.button << " / pointer x,y: " << event.xbutton.x << ", " << event.xbutton.y << endl;
            break;
        }

        case ButtonRelease:
        {
            m_btn[event.xbutton.button - 1] = 0;
            ++m_btnEventCnt;
            //cout << "mouse button release: " << event.xbutton.button << " / pointer x,y: " << event.xbutton.x << ", " << event.xbutton.y << endl;
            break;
        }

        case MotionNotify:
        {
            ++m_cursorEventCnt;
            m_cursor.set( event.xbutton.x, event.xbutton.y );
            //cout << "mouse move / abs = " << m_cursor.x << " x " << m_cursor.y << endl;//" / rel = " << m_dir[0] << " x " << m_dir[1] << endl;
            break;
        }

        case ColormapChangeMask:
        case EnterWindowMask:
        case ExposureMask:
        case FocusChangeMask:
        case KeymapStateMask:
        case LeaveWindowMask:
            break;
        case NoEventMask:
        case OwnerGrabButtonMask:
        case PointerMotionHintMask:
        case PointerMotionMask:
        case PropertyChangeMask:
        case ResizeRedirectMask:
        case StructureNotifyMask:
        case SubstructureNotifyMask:
        case SubstructureRedirectMask:
        case VisibilityChangeMask:
            break;
    }

    return *this;
}

bool GlxInput::key_down ( unsigned key ) const
{
    return m_key[key & 255];
}

bool GlxInput::btn ( unsigned btnNum ) const
{
    return m_btn[btnNum];
}

const Vertex2D<int>& GlxInput::cursor () const
{
    return m_cursor;
}
