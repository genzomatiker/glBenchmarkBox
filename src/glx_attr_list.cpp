/*******************************************************************************
* file   : glx_attr_list.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "glx_attr_list.h"
#include <GL/glx.h>

using namespace std;


GlxAttrList::GlxAttrList ()
{
}

GlxAttrList::GlxAttrList ( const int *pAttrArray )
{
    if ( pAttrArray )
        for ( unsigned idx = 0; None != pAttrArray[idx] && None != pAttrArray[idx+1]; idx += 2 )
            push_back( make_pair( pAttrArray[idx], pAttrArray[idx + 1] ) );
}

// e.g. (GLX_DOUBLEBUFFER, True)
GlxAttrList& GlxAttrList::add ( int attrCode, int attrValue )
{
    push_back( make_pair( attrCode, attrValue ) );
    return *this;
}

GlxAttrList& GlxAttrList::operator + ( GlxAttr attr )
{
    return add( attr.first, attr.second );
}

GlxAttrList& GlxAttrList::add_defaults ()
{
    add( GLX_X_RENDERABLE,  True );
    add( GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT );
    add( GLX_RENDER_TYPE  , GLX_RGBA_BIT );
    add( GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR );
    add( GLX_RED_SIZE,      8 );
    add( GLX_GREEN_SIZE,    8 );
    add( GLX_BLUE_SIZE,     8 );
    add( GLX_ALPHA_SIZE,    8 );
    add( GLX_DEPTH_SIZE,    24 );
    add( GLX_STENCIL_SIZE,  8 );
    add( GLX_DOUBLEBUFFER,  True );
    return *this;
}

int* GlxAttrList::create_int_list ()
{
    int *pAttrList = new int[2 * size() + 1];
    if ( pAttrList )
    {
        GlxAttrList::iterator itPair = begin();
        for ( unsigned idx = 0; idx < size(); ++idx, ++itPair )
        {
            pAttrList[2 * idx + 0] = itPair->first;
            pAttrList[2 * idx + 1] = itPair->second;
        }
        pAttrList[size() * 2] = None;
    }

    return pAttrList;
}
