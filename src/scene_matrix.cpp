/*******************************************************************************
* file   : scene_matrix.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "scene_matrix.h"
#include <GL/gl.h>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


// This class/entity exists to avoid NULL pointers and connected problems like
// segfaults. Instead of assigning NULL, a reference to a global object is done.
struct SceneIdle : public SceneElement
{
    virtual void            init () {}
    virtual void            render () const {}
} gIdleScene;

// =============================================================================
// = SceneMatrix members
// =============================================================================
SceneMatrix::SceneMatrix()
{
    init();
}

void SceneMatrix::init ()
{
    m_modus     = 0;
    p_scene     = &gIdleScene;
    scale       = Vertex3D<float>( 1, 1, 1 );
    rotate      = Vertex3D<float>( 0, 0, 0 );
    translate   = Vertex3D<float>( 0, 0, 0 );
}

void SceneMatrix::render () const
{
    glPushMatrix();

    // transform current matrix
    switch ( m_modus )
    {
        case 0b100:
            glScalef( scale.x, scale.y, scale.z );
            break;
        case 0b101:
            glScalef( scale.x, scale.y, scale.z );
            glTranslatef( translate.x, translate.y, translate.z );
            break;
        case 0b110:
            glScalef( scale.x, scale.y, scale.z );
            glRotatef( rotate.x, 1, 0, 0 );
            glRotatef( rotate.y, 0, 1, 0 );
            glRotatef( rotate.z, 0, 0, 1 );
            break;
        case 0b111:
            glScalef( scale.x, scale.y, scale.z );
            glRotatef( rotate.x, 1, 0, 0 );
            glRotatef( rotate.y, 0, 1, 0 );
            glRotatef( rotate.z, 0, 0, 1 );
            glTranslatef( translate.x, translate.y, translate.z );
            break;
        case 0b010:
            glRotatef( rotate.x, 1, 0, 0 );
            glRotatef( rotate.y, 0, 1, 0 );
            glRotatef( rotate.z, 0, 0, 1 );
            break;
        case 0b011:
            glTranslatef( translate.x, translate.y, translate.z );
            glRotatef( rotate.x, 1, 0, 0 );
            glRotatef( rotate.y, 0, 1, 0 );
            glRotatef( rotate.z, 0, 0, 1 );
            break;
        case 0b001:
            glTranslatef( translate.x, translate.y, translate.z );
            break;
        default:
            ;
    }

    // render child scene
    p_scene->render();

    glPopMatrix();
}

SceneMatrix& SceneMatrix::scene ( const SceneElement &scene )
{
    p_scene = &scene;
    return *this;
}

SceneMatrix& SceneMatrix::mode ( bool scale, bool rotate, bool translate )
{
    unsigned sBit = scale     ? 1 : 0;
    unsigned rBit = rotate    ? 1 : 0;
    unsigned tBit = translate ? 1 : 0;
    m_modus       = (sBit << 2) | (rBit << 1) | (tBit << 0);
    return *this;
}
