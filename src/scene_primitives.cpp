/*******************************************************************************
* file   : scene_primitives.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "scene_primitives.h"
#include <limits.h>
using namespace std;


// =============================================================================
// =               utilities                                                   =
// =============================================================================

#define INVALID_OFFS         UINT_MAX

unsigned elem_byte_count ( GLenum format )
{
    switch ( format )
    {
        case GL_V3F:
            return sizeof( float ) * 3;
        case GL_C3F_V3F:
        case GL_N3F_V3F:
            return sizeof( float ) * 6;
        case GL_C4F_N3F_V3F:
            return sizeof( float ) * 10;
        case GL_T2F_V3F:
            return sizeof( float ) * 5;
        case GL_T2F_N3F_V3F:
            return sizeof( float ) * 8;
        default: ;
    }

    return 0;
}

unsigned elem_color_offs ( GLenum format )
{
    switch ( format )
    {
        case GL_C3F_V3F:
        case GL_C4F_N3F_V3F:
            return 0;
        default: ;
    }

    return INVALID_OFFS;
}

unsigned elem_texel_offs ( GLenum format )
{
    switch ( format )
    {
        case GL_T2F_V3F:
        case GL_T2F_N3F_V3F:
            return 0;
        default: ;
    }

    return INVALID_OFFS;
}

unsigned elem_normal_offs ( GLenum format )
{
    switch ( format )
    {
        case GL_N3F_V3F:
            return 0;
        case GL_C4F_N3F_V3F:
            return sizeof( float ) * 4;
        case GL_T2F_N3F_V3F:
            return sizeof( float ) * 2;
        default: ;
    }

    return INVALID_OFFS;
}

unsigned elem_vertex_offs ( GLenum format )
{
    switch ( format )
    {
        case GL_V3F:
            return 0;
        case GL_C3F_V3F:
        case GL_N3F_V3F:
            return sizeof( float ) * 3;
        case GL_C4F_N3F_V3F:
            return sizeof( float ) * 7;
        case GL_T2F_V3F:
            return sizeof( float ) * 2;
        case GL_T2F_N3F_V3F:
            return sizeof( float ) * 5;
        default: ;
    }

    return INVALID_OFFS;
}


#include <iostream>
Vertex3D<float>& array_element ( unsigned idx, GLenum format, unsigned(*elem_offs_func)(unsigned), GLvoid *pMesh )
{
    unsigned elemOffs  = elem_byte_count( format );
    unsigned membOffs  = elem_offs_func( format );
    if ( INVALID_OFFS == elemOffs || INVALID_OFFS == membOffs )//|| element_count() < idx )
    {
        cerr << "error: accessing illegal element of primitive!" << endl;
        exit( EXIT_FAILURE );
    }

    GLbyte  *pMeshByte = reinterpret_cast<GLbyte*>( pMesh ) + elemOffs * idx + membOffs;
    return *reinterpret_cast<Vertex3D<float>*>( pMeshByte  );
}

// =============================================================================
// =               class members                                               =
// =============================================================================
// Ugly macro! But using a function is even more ugly because of the long signature.
#define ASSIGN_MEMBERS( elemCnt, mode, format )                                \
    m_meshType   = mode;                                                       \
    m_meshFormat = format;                                                     \
    m_meshStride = mode == GL_V3F ? 0 : elem_byte_count( format );             \
    m_indexCnt   = elemCnt;

ScenePrimitives::ScenePrimitives()
{
    p_mesh       = nullptr;
    p_indexes    = nullptr;
    ASSIGN_MEMBERS( 0, GL_INVALID_ENUM, GL_INVALID_ENUM )
}

bool ScenePrimitives::init ( unsigned elemCnt, GLenum mode, GLenum format )
{
    unsigned elemBytes = elem_byte_count( format );
    if ( 0 < elemBytes && !p_mesh && !p_indexes )
    {
        p_mesh = new GLbyte[elemBytes * elemCnt];
        if ( !p_mesh )
            return false;

        p_indexes = new unsigned[elemCnt];
        if ( !p_indexes )
        {
            delete (GLbyte*)p_mesh;
            return false;
        }

        ASSIGN_MEMBERS( elemCnt, mode, format )
    }

    return true;
}

// =============================================================================
// =               accessing members                                           =
// =============================================================================

GLenum ScenePrimitives::mode () const
{
    return m_meshType;
}

GLenum ScenePrimitives::format () const
{
    return m_meshFormat;
}

// rename into index_count()
unsigned ScenePrimitives::element_count () const
{
    return m_indexCnt;
}

// =============================================================================
// =               accessing mesh members                                      =
// =============================================================================

unsigned& ScenePrimitives::index ( unsigned idx )
{
    return *(p_indexes + idx);
}

Vertex3D<float>& ScenePrimitives::color ( unsigned idx )
{
    return array_element( idx, m_meshFormat, elem_color_offs, p_mesh );
}

Vertex3D<float>& ScenePrimitives::texel ( unsigned idx )
{
    return array_element( idx, m_meshFormat, elem_texel_offs, p_mesh );
}

Vertex3D<float>& ScenePrimitives::normal ( unsigned idx )
{
    return array_element( idx, m_meshFormat, elem_normal_offs, p_mesh );
}

Vertex3D<float>& ScenePrimitives::vertex ( unsigned idx )
{
    return array_element( idx, m_meshFormat, elem_vertex_offs, p_mesh );
}

// =============================================================================
// =               render functions                                            =
// =============================================================================

void ScenePrimitives::render () const
{
    glInterleavedArrays( m_meshFormat, m_meshStride, p_mesh );
    glDrawElements( m_meshType, m_indexCnt, GL_UNSIGNED_INT, p_indexes );
}

// =============================================================================
// =               special render functions for benchmarking                   =
// =============================================================================

void ScenePrimitives::register_array ()
{
    glInterleavedArrays( m_meshFormat, m_meshStride, p_mesh );
}

void ScenePrimitives::render_glBeginEnd ()
{
    glBegin( GL_TRIANGLES );
        for ( unsigned vIdx = 0; vIdx < m_indexCnt; ++vIdx )
            glVertex3fv( &vertex(vIdx).x );
            ;
    glEnd();
}

void ScenePrimitives::render_elements ()
{
    glDrawElements( m_meshType, m_indexCnt, GL_UNSIGNED_INT, p_indexes );
}

void ScenePrimitives::render_array ()
{
    glDrawArrays( m_meshType, 0, m_indexCnt );
}
