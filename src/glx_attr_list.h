/*******************************************************************************
* file   : glx_attr_list.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef glx_attr_list_H
#define glx_attr_list_H

#include <tuple>
#include <vector>

typedef std::pair<int,int>      GlxAttr;


// Simplifies openGL attribute definitions & transformations into int array.
class GlxAttrList : public std::vector<GlxAttr>
{
public:
                                GlxAttrList ();
                                GlxAttrList ( const int *pAttrArray );
    GlxAttrList&                add ( int attrCode, int attrValue );
    GlxAttrList&                operator + ( GlxAttr attr );
    GlxAttrList&                add_defaults ();
    int*                        create_int_list ();
};

#endif  // glx_attr_list_H
