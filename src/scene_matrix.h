/*******************************************************************************
* file   : scene_matrix.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef scene_matrix_H
#define scene_matrix_H

#include "scene_element.h"
#include "vertex_3d.h"


struct SceneMatrix : public SceneElement
{
public:
                            SceneMatrix ();
    virtual void            init ();
    virtual void            render () const;
    SceneMatrix&            scene ( const SceneElement &scene );
    SceneMatrix&            mode ( bool scale, bool rotate, bool translate );

public:
    Vertex3D<float>                 scale;
    Vertex3D<float>                 rotate;
    Vertex3D<float>                 translate;

protected:
    unsigned                        m_modus;
    const SceneElement*             p_scene;
};

#endif   // scene_matrix_H
