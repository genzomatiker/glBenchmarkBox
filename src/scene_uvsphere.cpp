/*******************************************************************************
* file   : scene_uvsphere.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "scene_uvsphere.h"
#include <GL/gl.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include "tracing.h" // <---- only for trace/debug purposes -> remove

using namespace std;

// =============================================================================
// =               sphere geometric details                                    =
// =============================================================================
namespace sphere
{
    typedef Vertex3D<float>     Vertex3f;
    unsigned                    faceEdgeCnt = 3;


    vector<Vertex3f> vertex_list ( unsigned xVertCnt, unsigned yVertCnt )
    {
        vector<Vertex3D<float> > vertices;
        for ( unsigned ySect = 0; ySect < yVertCnt; ++ySect )
            for ( unsigned xSect = 0; xSect < xVertCnt; ++xSect )
            {
                float zDeg = ySect * 180 / yVertCnt + 180 / yVertCnt / 2;
                float yDeg = xSect * 360 / xVertCnt + 360 / xVertCnt / 2;
                vertices.push_back( Vertex3D<float>( 0, 0.5, 0 ).rotate_z( zDeg ).rotate_y( yDeg ) );
            }

        vertices.push_back( Vertex3D<float>( 0, 0.5, 0 ) );
        vertices.push_back( Vertex3D<float>( 0, -0.5, 0 ) );
        return vertices;
    }

    unsigned index ( unsigned xVertCnt, unsigned yVertCnt, unsigned xIdx, unsigned yIdx )
    {
        return xIdx % xVertCnt + yIdx * xVertCnt;
    }

    vector<unsigned> index_list ( unsigned xVertCnt, unsigned yVertCnt )
    {
        vector<unsigned> triangles;

        // compute body face indexes
        for ( unsigned ySect = 0; ySect < yVertCnt-1; ++ySect )
            for ( unsigned xSect = 0; xSect < xVertCnt; ++xSect )
            {
                triangles.push_back( index(xVertCnt,yVertCnt,xSect,ySect) );
                triangles.push_back( index(xVertCnt,yVertCnt,xSect,ySect+1) );
                triangles.push_back( index(xVertCnt,yVertCnt,xSect+1,ySect+1) );

                triangles.push_back( index(xVertCnt,yVertCnt,xSect+1,ySect+1) );
                triangles.push_back( index(xVertCnt,yVertCnt,xSect+1,ySect) );
                triangles.push_back( index(xVertCnt,yVertCnt,xSect,ySect) );
            }

        // compute faces from most top and bottom x-ring
        for ( unsigned xSect = 0; xSect < xVertCnt; ++xSect )
        {
            triangles.push_back( index(xVertCnt,yVertCnt,0,yVertCnt) );
            triangles.push_back( index(xVertCnt,yVertCnt,xSect,0) );
            triangles.push_back( index(xVertCnt,yVertCnt,(xSect + 1) % xVertCnt,0) );
            triangles.push_back( index(xVertCnt,yVertCnt,(xSect + 1) % xVertCnt,yVertCnt-1) );
            triangles.push_back( index(xVertCnt,yVertCnt,xSect,yVertCnt-1) );
            triangles.push_back( index(xVertCnt,yVertCnt,1,yVertCnt) );
        }

        return triangles;
    }

    vector<Vertex3f> flat_normal_list ( const vector<Vertex3f> &vertices, const vector<unsigned> &vertIdx )
    {
        vector<Vertex3f> flatNormals;
        unsigned faceEdges = faceEdgeCnt;
        for ( unsigned vertIdxNum = 0; vertIdxNum < vertIdx.size(); vertIdxNum += faceEdges )
        {
            Vertex3D<float> fV1    = vertices[vertIdx[vertIdxNum+1]] - vertices[vertIdx[vertIdxNum]];
            Vertex3D<float> fV2    = vertices[vertIdx[vertIdxNum+2]] - vertices[vertIdx[vertIdxNum]];
            Vertex3D<float> normal = fV2 % fV1;
            flatNormals.push_back( normal.normalize() );
        }

        return flatNormals;
    }

    vector<unsigned> sphere_flat_normal_indexes ( unsigned faceCnt )
    {
        vector<unsigned> normalIndexes;
        for ( unsigned faceIdx = 0; faceIdx < faceCnt; ++faceIdx )
            normalIndexes.push_back( faceIdx );

        return normalIndexes;
    }

    vector<Vertex3f> color_list ( unsigned vertCnt, Vertex3f topCol, Vertex3f botCol )
    {
        Vertex3f col;
        vector<Vertex3f> vertexColors;
        for ( unsigned idx = 0; idx < vertCnt; ++idx )
        {
            float yRing = idx == vertCnt-2 ? 0 : (float)idx;
            col.x = topCol.x + (botCol.x - topCol.x) * yRing / vertCnt;
            col.y = topCol.y + (botCol.y - topCol.y) * yRing / vertCnt;
            col.z = topCol.z + (botCol.z - topCol.z) * yRing / vertCnt;
            vertexColors.push_back( col );
        }

        return vertexColors;
    }
}

using namespace sphere;


// =============================================================================
// =               class members                                               =
// =============================================================================
SceneUvSphere::SceneUvSphere ( unsigned vertexCnt, Vertex3f tCol, Vertex3f bCol ) : ScenePrimitives()
{
    m_colRange[0] = tCol;
    m_colRange[1] = bCol;
    m_indexCnt    = vertexCnt;
    init();
}

// assign vertices, colors, normals and indexes
bool SceneUvSphere::init ()
{
    unsigned          xVertCnt      = sqrtf( element_count() );
    unsigned          yVertCnt      = element_count() / xVertCnt;
    vector<Vertex3f>  vertices      = vertex_list( xVertCnt, yVertCnt );
    vector<unsigned>  indexes       = index_list( xVertCnt, yVertCnt );
    vector<Vertex3f>  smoothNormals = vertices;
//    vector<Vertex3f>  flatNormals   = flat_normal_list( vertices, indexes );
//    vector<unsigned>  flatNormIdx   = sphere_flat_normal_indexes( indexes.size() / faceEdgeCnt );
    vector<Vertex3f>  colors        = color_list( vertices.size(), m_colRange[0], m_colRange[1] );

    // =========================================================================
    // instantiate members
    unsigned elemCnt = indexes.size();
    bool allocOK     = ScenePrimitives::init( elemCnt, GL_TRIANGLES, GL_C4F_N3F_V3F );
    if ( allocOK )
        for ( unsigned idx = 0; idx < m_indexCnt; ++idx )
        {
            color( idx )[0] = colors[ indexes[idx] ].x;
            color( idx )[1] = colors[ indexes[idx] ].y;
            color( idx )[2] = colors[ indexes[idx] ].z;
            color( idx )[3] = 1;
            normal( idx )   = vertices[ indexes[idx] ];
            vertex( idx )   = vertices[ indexes[idx] ];
            index( idx )    = idx;
        }

    return allocOK;
}
