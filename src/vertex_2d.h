/*******************************************************************************
* file   : vertex_2d.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef vertex_2d_H
#define vertex_2d_H


// offers an easy interface to Vertex array members and make indexing unnecessary
template<typename Type>
struct Vertex2D
{
public:
                                Vertex2D ( Type defVal = 0 );
                                Vertex2D ( Type x, Type y );
                                Vertex2D ( const Vertex2D<Type> &cpy );
    Vertex2D<Type>&             operator = ( const Vertex2D<Type> &cpy );
    Type&                       operator [] ( unsigned idx );
    Vertex2D<Type>&             set ( Type x, Type y );
    // -------------------------------------------------------------------------
    Vertex2D<Type>              operator + ( const Vertex2D<Type> &add ) const;
    Vertex2D<Type>              operator - ( const Vertex2D<Type> &sub ) const;
    Vertex2D<Type>              operator * ( const Vertex2D<Type> &mul ) const;
    Vertex2D<Type>              operator / ( const Vertex2D<Type> &div ) const;
    // -------------------------------------------------------------------------
    Vertex2D<Type>              operator + ( const Type           &add ) const;
    Vertex2D<Type>              operator - ( const Type           &sub ) const;
    Vertex2D<Type>              operator * ( const Type           &mul ) const;
    Vertex2D<Type>              operator / ( const Type           &div ) const;
    // -------------------------------------------------------------------------
    Vertex2D<Type>              rotate_x ( float degrees );
    Vertex2D<Type>              rotate_y ( float degrees );
    Vertex2D<Type>              rotate_z ( float degrees );
    // -------------------------------------------------------------------------
    Type                        length ();
    Vertex2D<Type>              normalize ( float len = 1 );

public:
    Type                            x;
    Type                            y;
};

// template implementation must be included by declaring header file
#include "vertex_2d.cpp.h"

#endif   // vertex_2d_H
