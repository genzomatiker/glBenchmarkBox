/*******************************************************************************
* file   : glx_window.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef glx_window_H
#define glx_window_H

#include "error_counter.h"
#include "glx_display.h"
#include "vertex_2d.h"
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <GL/glx.h>


// Simplifies display, window, keyboard, and error management of the Xlib API.
class GlxWindow : public ErrorCounter
{
public:
                            GlxWindow ( GlxDisplay &dspl, int x, int y, unsigned w, unsigned h );
                            GlxWindow ( GlxDisplay &dspl, int x, int y, unsigned w, unsigned h, XVisualInfo &visuaInfo );
                            GlxWindow ( GlxDisplay &dspl, int x, int y, unsigned w, unsigned h, XVisualInfo &visuaInfo, GLXFBConfig pFbConfig );
                           ~GlxWindow ();
    GlxWindow&              create_glx_context ();
    GLXContext              create_glx_context ( int glxMajVer, int glxMinVer );
    GlxWindow&              show ( bool visible );
    GlxWindow&              title ( const char *pTitle );
    GlxWindow&              swap_buffers ();
    GlxWindow&              clear ();
    GlxDisplay&             display ();
    Window                  window ();
    const Vertex2D<int>&    geometry () const;
    const Vertex2D<int>&    position () const;

protected:
    unsigned                        m_xErrCnt;          // error counter
    GlxDisplay&                     m_display;          // X11 display server
    Window                          m_window;           // X11 display
    const char*                     p_title;            // window title
    Vertex2D<int>                   m_geometry;         // windows width & height
    Vertex2D<int>                   m_position;         // windows left upper corner
    GLXFBConfig                     p_fbConfig;
    GLXContext                      p_context;
};

#endif  // glx_window_H
