/*******************************************************************************
* file   : vertex_3d.cpp.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "vertex_3d.h"
#include <math.h>


// -----------------------------------------------------------------------------
// - local tool functions
// -----------------------------------------------------------------------------
inline float deg2rad ( float deg )
{
    return float(deg) * float(M_PI) / float(180);
}

// -----------------------------------------------------------------------------
// - member functions
// -----------------------------------------------------------------------------
template<typename Type>
Vertex3D<Type>::Vertex3D ( Type defVal )
{
    set( defVal, defVal, defVal );
}

template<typename Type>
Vertex3D<Type>::Vertex3D ( Type X, Type Y, Type Z )
{
    set( X, Y, Z );
}

template<typename Type>
Vertex3D<Type>::Vertex3D ( const Vertex3D<Type> &cpy )
{
    *this = cpy;
}

template<typename Type>
Vertex3D<Type>& Vertex3D<Type>::operator = ( const Vertex3D<Type> &cpy )
{
    set( cpy.x, cpy.y, cpy.z );
    return *this;
}

template<typename Type>
bool Vertex3D<Type>::operator == ( const Vertex3D<Type> &cmp )
{
    return x == cmp.x & y == cmp.y & z == cmp.z;
}

template<typename Type>
Type& Vertex3D<Type>::operator [] ( unsigned idx )
{
    Type *pArray = &x;
    return pArray[idx];
}

template<typename Type>
Vertex3D<Type>& Vertex3D<Type>::set ( Type X, Type Y, Type Z )
{
    x = X;
    y = Y;
    z = Z;
    return *this;
}

template<typename Type>
Vertex3D<Type> Vertex3D<Type>::operator + ( const Vertex3D<Type> &add ) const
{
    return Vertex3D<Type>( x + add.x, y + add.y, z + add.z );
}

template<typename Type>
Vertex3D<Type> Vertex3D<Type>::operator - ( const Vertex3D<Type> &sub ) const
{
    return Vertex3D<Type>( x - sub.x, y - sub.y, z - sub.z );
}

template<typename Type>
Vertex3D<Type> Vertex3D<Type>::operator * ( const Vertex3D<Type> &mul ) const
{
    return Vertex3D<Type>( x * mul.x, y * mul.y, z * mul.z );
}

template<typename Type>
Vertex3D<Type> Vertex3D<Type>::operator / ( const Type &div ) const
{
    return Vertex3D<Type>( x / div, y / div, z / div );
}

// vector cross product
template<typename Type>
Vertex3D<Type> Vertex3D<Type>::operator % ( const Vertex3D<Type> &mul ) const
{
    return Vertex3D<Type>( y*mul.z - z*mul.y, z*mul.x - x*mul.z, x*mul.y - y*mul.x );
}

template<typename Type>
Vertex3D<Type> Vertex3D<Type>::operator * ( const Type &mul ) const
{
    return Vertex3D<Type>( x * mul, y * mul, z * mul );
}

template<typename Type>
Vertex3D<Type> Vertex3D<Type>::rotate_x ( float deg )
{
    float  rad    = deg2rad( deg );
    float  sinRad = sinf( rad );
    float  cosRad = cosf( rad );
    return Vertex3D<Type>( x,
                           cosRad*y - sinRad*z,
                           sinRad*y + cosRad*z );
}

template<typename Type>
Vertex3D<Type> Vertex3D<Type>::rotate_y ( float deg )
{
    float  rad    = deg2rad( deg );
    float  sinRad = sinf( rad );
    float  cosRad = cosf( rad );
    return Vertex3D<Type>( cosRad*x + sinRad*z,
                           y,
                          -sinRad*x + cosRad*z );
}

template<typename Type>
Vertex3D<Type> Vertex3D<Type>::rotate_z ( float deg )
{
    float  rad    = deg2rad( deg );
    float  sinRad = sinf( rad );
    float  cosRad = cosf( rad );
    return Vertex3D<Type>( cosRad*x - sinRad*y,
                           sinRad*x + cosRad*y,
                           z );
}

template<typename Type>
Type Vertex3D<Type>::length ()
{
    return sqrtf( x * x + y * y + z * z );
}

template<typename Type>
Vertex3D<Type> Vertex3D<Type>::normalize ( float len )
{
    return *this * len / length();
}
