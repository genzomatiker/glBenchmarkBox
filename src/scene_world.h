/*******************************************************************************
 * file   : scene_world.h
 * author : Christian Genz
 * email  : genzomatiker@gmail.com
 * info   :
 *******************************************************************************/

#ifndef scene_world_H
#define scene_world_H


#include "scene.h"
#include "scene_matrix.h"
#include "scene_primitives.h"
#include "scene_light.h"


class SceneWorld
{
public:
                    SceneWorld ();
                   ~SceneWorld ();
    bool            init ( unsigned sphereCnt, unsigned faceCnt, unsigned w, unsigned h, unsigned meshTyp = 0 );
    bool            init_camera ();
    bool            init_sky_box ();
    bool            init_colliders ( unsigned sphereCnt, unsigned faceCnt, unsigned meshTyp );
    bool            init_debugging ();
    bool            init_light_bulp ();

protected:
    Scene*                          p_scene;        // world scene
    ScenePrimitives*                p_colliderMesh; // model (able to collide)
    Scene*                          p_collider;     // list of model matrices able to collide
    Scene*                          p_debugging;    // model list rendered for debugging
    SceneMatrix*                    p_lightBulp;    // matrix pointing to model symbolizing light
    SceneLight*                     p_light;        // light configuration
    SceneMatrix*                    p_cam;          // camera matrix for p_scene
};

#endif  // scene_world_H
