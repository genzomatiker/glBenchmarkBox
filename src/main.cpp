// =============================================================================
// file   : main.cpp
// author : Christian Genz
// email  : genzomatiker@gmail.com
// =============================================================================

#include <stdlib.h>
#include <iostream>
#include "arg_list.h"
#include "collision_box.h"

using namespace std;


// applications start point
int main ( int argc, const char *argv[] )
{
    unsigned optHlp    = 0;
    unsigned optVer    = 0;
    unsigned optMeshs  = 9;
    unsigned optFaces  = 512;
    unsigned optWinW   = 1024;
    unsigned optWinH   = 768;
    unsigned optMesh   = 0;
    bool     optLog    = false;

    // process arguments and cast values to required types & formats
    ArgList argList;
    argList.set_help( "test_arglist {OPTIONS}\nOPTIONS are:" );
    argList["--help"].help( "Print this help message." )                                 > optHlp;
    argList["-ver"].help(   "Print version and exit." )                                  > optVer;
    argList["-meshes"].help( "Sets the number of meshes to be rendered." )              >> optMeshs;
    argList["-faces"].help( "Sets the number of triangles per mesh to be rendered." )   >> optFaces;
    argList["-width"].help( "Sets the window width." )                                  >> optWinW;
    argList["-height"].help( "Sets the window height." )                                >> optWinH;
    argList["-mesh"].help( "Sets colliders mesh (0/1/2/3 = ico|uv-sphere/cube/wave)." ) >> optMesh;
    argList["-log"].help( "Activates console logging (default = off)." )                >> optLog;
    argList.apply_rules( argc, argv );

    // handle errors and argument specific functionality
    if ( optHlp )
        cout << argList.get_help() << endl;
    else if ( !argList.illegal_args().empty() )
        cout << argList.illegal_args_error( true ) << endl;
    else if ( optVer )
        cout << argv[0] << " build date: " << __DATE__ << " / " << __TIME__ << endl;

    // configure, initialize & run game engine
    CollisionBox box;
    bool argOK = argList.illegal_args().empty();
    bool abort = !argOK || optHlp || optVer;
    bool iniOK = !abort && box.init( optMeshs, optFaces, optWinW, optWinH, optMesh );
    bool runOK = iniOK  && box.run();

    // terminate application
    return argOK && (abort || runOK) ? EXIT_SUCCESS : EXIT_FAILURE;
}
