/*******************************************************************************
* file   : glx_xvisual_info.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "glx_xvisual_info.h"
#include <iostream>

using namespace std;


// release config list memory, allocated by glXChooseVisual()
GlxXVisualInfo::~GlxXVisualInfo ()
{
    if ( p_visInfo )
        XFree( p_visInfo );
}

// GLX >= 1.4 (openGL 3.0)
GlxXVisualInfo::GlxXVisualInfo ( GlxDisplay &dspl, GLXFBConfig &conf ) : ErrorCounter( m_vErrCnt ),
                                                                         m_display( dspl )
{
    p_visInfo = glXGetVisualFromFBConfig( dspl.display(), conf );
    if ( !p_visInfo )
    {
        cerr << "error: failed to query GLX visual info configurations!" << endl;
        ++errors();
    }
}

// GLX <= 1.1 (openGL 1.3)
GlxXVisualInfo::GlxXVisualInfo ( GlxDisplay &dspl, int scr, GlxAttrList &attrList ) : ErrorCounter( m_vErrCnt ),
                                                                                      m_display( dspl )
{
    p_visInfo = NULL;

    // find all FB attr configs that match the required attributes
    int *pAttrList = attrList.create_int_list();
    if ( pAttrList )
    {
        p_visInfo = glXChooseVisual( dspl.display(), scr, pAttrList );
        if ( p_visInfo )
        {
            //glXGetConfig( dspl, pVInfo, attrCode, &attrVal );  <-- this line could become handy if I have additional attributes to check for
        }
        else
        {
            cerr << "error: failed to query GLX visual info configurations!" << endl;
            ++errors();
        }
    }
}

XVisualInfo* GlxXVisualInfo::get_info ()
{
    return p_visInfo;
}
