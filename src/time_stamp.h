/*******************************************************************************
* file   : time_stamp.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef time_stamp_H
#define time_stamp_H

#include <sys/time.h>


struct TimeStamp
{
                            TimeStamp ();
    TimeStamp&              update ();
    unsigned                sec  () const;
    unsigned                usec () const;
    unsigned long           total () const;
    unsigned long           operator - ( const TimeStamp &sub ) const;

protected:
    timeval                         m_timeStamp;
};

#endif   // time_stamp_H
