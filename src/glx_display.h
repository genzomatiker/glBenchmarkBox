/*******************************************************************************
* file   : glx_display.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef glx_display_H
#define glx_display_H

#include "error_counter.h"
#include <X11/Xlib.h>
#include <string>
#include <utility>


// Simplifies display and error management of the Xlib and GLX API.
class GlxDisplay : public ErrorCounter
{
public:
    typedef int                 (*XlibHandler) ( Display*, XErrorEvent* );

public:
                                GlxDisplay ( const char *pName = nullptr );
                               ~GlxDisplay ();

    GlxDisplay&                 flush ();
    GlxDisplay&                 sync ();
    Display*                    display ();
    int                         screen ();
    unsigned long               black_pixel ();
    unsigned long               white_pixel ();
    Window                      root_win ();
    std::pair<int,int>          version ();
    const char*                 supported_functions ();
    bool                        supported_function ( const std::string &name );
    const char*                 server_string ( int name );
    const char*                 client_string ( int name );
    static int                  error_handler ( Display*, XErrorEvent* );

protected:
    unsigned                        m_xErrCnt;        // error counter
    XlibHandler                     p_xErrHandler;    // X11 error handler backup
    int                             m_glxVer[2];      // major & minor GLX version
    const char*                     p_displayName;    // connected server name
    Display*                        p_display;        // connected display server
};

#endif  // glx_display_H
