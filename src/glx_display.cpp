/*******************************************************************************
* file   : glx_display.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "glx_display.h"
#include <GL/glx.h>
#include <iostream>
#include <sstream>

using namespace std;


// connect to X11 server & install error handler
GlxDisplay::GlxDisplay ( const char *pName ) : ErrorCounter( m_xErrCnt )
{
    m_xErrCnt     = 0;
    p_xErrHandler = XSetErrorHandler( GlxDisplay::error_handler );
    m_glxVer[0]   = 0;
    m_glxVer[1]   = 0;
    p_displayName = pName;
    p_display     = XOpenDisplay( pName );
    if ( !p_display )
    {
        cerr << "error: unable to open display" << endl;
        ++errors();
    }
    else
    {
        // query openGL version
        bool verErr = !glXQueryVersion( p_display, &m_glxVer[0], &m_glxVer[1] );
        if ( verErr )
        {
            cerr << "error: unable to query openGL version" << endl;
            ++errors();
        }
    }
}

// close X11 server connection & reinstall previous error handler
GlxDisplay::~GlxDisplay ()
{
    if ( p_display )
    {
        XCloseDisplay( p_display );
        XSetErrorHandler( p_xErrHandler );
        p_display     = nullptr;
        p_xErrHandler = nullptr;
    }
}

// flush the output buffer (not required when calling XNextEvent instead)
GlxDisplay& GlxDisplay::flush ()
{
    XFlush( p_display );
    return *this;
}


// sync the output buffer (not required when calling XNextEvent instead)
GlxDisplay& GlxDisplay::sync ()
{
    XSync( p_display, False );
    return *this;
}

// returns a pointer to the connected display server
Display* GlxDisplay::display ()
{
    return p_display;
}

int GlxDisplay::screen ()
{
    return DefaultScreen( p_display );
}

unsigned long GlxDisplay::black_pixel ()
{
    return BlackPixel( p_display, screen() );
}

unsigned long GlxDisplay::white_pixel ()
{
    return WhitePixel( p_display, screen() );
}

Window GlxDisplay::root_win ()
{
    return DefaultRootWindow( p_display );
}

pair<int,int> GlxDisplay::version ()
{
    return make_pair( m_glxVer[0], m_glxVer[1] );
}

const char* GlxDisplay::supported_functions ()
{
    return glXQueryExtensionsString( p_display, screen() );
}

bool GlxDisplay::supported_function ( const string &func )
{
    istringstream glxFuncs( supported_functions() );
    string        word;
    for ( ; func != word && glxFuncs; )
        glxFuncs >> word;

    return func == word;
}

const char* GlxDisplay::server_string ( int name )
{
    return glXQueryServerString( p_display, DefaultScreen(p_display), name );
}

const char* GlxDisplay::client_string ( int name )
{
    return glXGetClientString( p_display, name );
}

// Xlib callback function (used for error management)
int GlxDisplay::error_handler ( Display *pDisplay, XErrorEvent *pError )
{
    char msg[1024];
    XGetErrorText( pDisplay, pError->error_code, msg, sizeof(msg) );
    cerr << "error: X11 display error occurred (" << msg << ")" << endl;

    switch ( pError->error_code )
    {
        case BadAccess:
        case BadAlloc:
        case BadAtom:
        case BadColor:
        case BadCursor:
        case BadDrawable:
        case BadFont:
        case BadGC:
        case BadIDChoice:
        case BadImplementation:
        case BadLength:
        case BadMatch:
        case BadName:
        case BadPixmap:
        case BadWindow:
            // NOTE: not handled yet!
        default: ;
    }

    return 0;
}
