/*******************************************************************************
* file   : scene.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "scene.h"


Scene::Scene ()
{
}

void Scene::init ()
{
}

void Scene::render () const
{
    unsigned elemCnt = size();
    unsigned idx     = 0;
    while ( idx ^ elemCnt )
        (*this)[idx++]->render();
}
