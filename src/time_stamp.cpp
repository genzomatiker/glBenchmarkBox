/*******************************************************************************
* file   : time_stamp.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "time_stamp.h"

using namespace std;

#define USEC_PER_SEC       1000000


TimeStamp::TimeStamp ()
{
    update();
}

TimeStamp& TimeStamp::update ()
{
    gettimeofday( &m_timeStamp, nullptr );
    return *this;
}

unsigned TimeStamp::sec () const
{
    return m_timeStamp.tv_sec;
}

unsigned TimeStamp::usec () const
{
    return m_timeStamp.tv_usec;
}

unsigned long TimeStamp::total () const
{
    return m_timeStamp.tv_sec * USEC_PER_SEC + m_timeStamp.tv_usec;
}

unsigned long TimeStamp::operator - ( const TimeStamp &sub ) const
{
    unsigned long sec  = (unsigned long)(m_timeStamp.tv_sec)  - (unsigned long)(sub.m_timeStamp.tv_sec);
    unsigned long usec = (unsigned long)(m_timeStamp.tv_usec) - (unsigned long)(sub.m_timeStamp.tv_usec);
    return sec * USEC_PER_SEC + usec;
}
