/*******************************************************************************
* file   : scene_debugger.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef scene_debugger_H
#define scene_debugger_H

#include "scene_primitives.h"


struct SceneDebugger : public ScenePrimitives
{
                            SceneDebugger ( const ScenePrimitives &primitives );
    bool                    init ( const ScenePrimitives &primitives );
};

#endif   // scene_debugger_H
