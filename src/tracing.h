/*******************************************************************************
* file   : tracing.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef tracing_H
#define tracing_H

#include "vertex_3d.h"
#include <vector>


    template<typename T>
    std::ostream& operator << ( std::ostream &dst, const Vertex3D<T> &vert );

    template<typename T>
    std::ostream& operator << ( std::ostream &dst, const std::vector<Vertex3D<T> > &vertList );


// template implementation must be included by declaring header file
#include "tracing.cpp.h"

#endif   // tracing_H
