/*******************************************************************************
* file   : scene_waves.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef scene_waves_H
#define scene_waves_H

#include "scene_primitives.h"


struct SceneWaves : public ScenePrimitives
{
                            SceneWaves ( float w, float h, unsigned triCnt, GLenum format );
    bool                    init ( GLenum format );
    void                    init_normals_smooth ();
    void                    init_normals_flat ();

protected:
    Vertex3D<float>                 m_size;
    unsigned                        m_triCnt;
    Vertex3D<float>                 m_waveSrc;
    float                           m_waveCnt;
    float                           m_waveMax;
};

#endif   // scene_waves_H
