/*******************************************************************************
 * file   : collision_box.cpp
 * author : Christian Genz
 * email  : genzomatiker@gmail.com
 * info   :
 *******************************************************************************/

#include "collision_box.h"
#include <iostream>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <climits>
#include "glx_attr_list.h"
#include "glx_fb_conf_list.h"
#include "glx_xvisual_info.h"
#include "scene.h"
#include "scene_cube.h"
#include "scene_icosphere.h"
#include "scene_light.h"
#include "tracing.h"
#include <unistd.h>

using namespace std;

#define MICROSEC_PER_SEC       (1000000ul)

// ******************************************************************************
// ********************* functions **********************************************
// ******************************************************************************

CollisionBox::CollisionBox ()
{
    m_run         = false;
    p_display     = nullptr;
    p_window      = nullptr;
    p_input       = nullptr;
}

CollisionBox::~CollisionBox ()
{
    if ( p_window )
    {
        delete p_input;
        delete p_window;
        delete p_display;
    }
}

#define GET_GL_STRING( GL_STRING_ID )                                          \
    (glGetString( GL_STRING_ID ) ? (const char*)glGetString( GL_STRING_ID ) : "<unknown>")

void CollisionBox::dump_api_info ( ostream &dst )
{
    cout << "info: openGL API version  = " << GET_GL_STRING(GL_VERSION) << endl;
    cout << "info: openGL API vendor   = " << GET_GL_STRING( GL_VENDOR ) << endl;
    cout << "info: openGL API renderer = " << GET_GL_STRING( GL_RENDER ) << endl;
    cout << "info: openGL API shading language = " << GET_GL_STRING( GL_SHADING_LANGUAGE_VERSION ) << endl;
}

void config_shader ()
{
    // HW back face culling to boost performance
    glEnable( GL_CULL_FACE );
    glCullFace( GL_BACK );
    glFrontFace( GL_CCW );


    // hidden surface removal via z-buffer is a must for performance reasons
    glEnable( GL_DEPTH_TEST );

    // Goroud shading
    glShadeModel( GL_SMOOTH ); // GL_SMOOTH, GL_FLAT

    glMatrixMode( GL_MODELVIEW );

    // configure line width for debugging models
    glLineWidth( 4 );
}

unsigned elem_byte_count ( GLenum format );

// setup render context
bool CollisionBox::init ( unsigned sphereCnt, unsigned faceCnt, unsigned w, unsigned h, unsigned meshTyp )
{
    m_lastCycle    = 0;
    m_cycleTime    = 0;
    m_inputState   = 0;
    m_handleState  = 0;
    m_inputTimer   = 0;
    m_statsTimer   = 0;
    m_statsLen     = 0;
    m_cycleTimeMin = UINT_MAX;
    m_cycleTimeMax = 0;
    m_cycleTimeAvg = 0;
    // -------------------------------------------------------------------------
    bool winOK   = init_window( w, h );
    bool SceneOK = winOK && SceneWorld::init( sphereCnt, faceCnt, w, h, meshTyp );

    // print numbers in groups of 1000
    setlocale( LC_ALL,"" );
    std::locale::global( std::locale("") );
    std::cout.imbue( std::locale() );

    // dump scenario details
    cout << "info: mesh triangles per scene = " << p_collider->size() * p_colliderMesh->element_count() / 3 << endl;
    cout << "info: mesh entities per scene  = " << p_collider->size() << endl;
    cout << "info: mesh data size           = " << p_collider->size() * p_colliderMesh->element_count() * elem_byte_count( p_colliderMesh->format() ) / 1024 / 1024 << "MB" << endl;

    if ( winOK && SceneOK )
        config_shader();

    return winOK && SceneOK;
}

bool CollisionBox::init_window ( unsigned w, unsigned h )
{
    p_display = new GlxDisplay();
    if ( !p_display || !*p_display )
    {
        cerr << "error: can't initialize X11 display." << endl;
        return false;
    }

    int screenCnt = ScreenCount( p_display->display() );
    if ( screenCnt < 1 )
    {
        cerr << "error: no X11 screens found." << endl;
        return false;
    }
    else
        cout << "info: display contains " << screenCnt << " screen(s) / id = " << DefaultScreen( p_display->display() ) << endl;

    int            screen = DefaultScreen( p_display->display() );
    GlxAttrList    fbAttr;
    GlxFbConfList  fbCfgList( *p_display, screen, fbAttr.add_defaults() );
    if ( fbCfgList.empty() )
    {
        cerr << "error: can't initialize GLX frame buffer configuration." << endl;
        return false;
    }
    else
        cerr << "info: found " << fbCfgList.size() << " GLX visual info configurations." << endl;

    GlxXVisualInfo visualInfo( *p_display, fbCfgList.front() );
    if ( !visualInfo )
    {
        cerr << "error: can't initialize GLX visual info." << endl;
        return false;
    }

    p_window = new GlxWindow( *p_display, 100, 100, w, h, *visualInfo.get_info(), fbCfgList.front() );
    if ( !p_window || !*p_window )
    {
        cerr << "error: can't initialize X11 window." << endl;
        return false;
    }

    p_input = new GlxInput( *p_window );
    if ( !p_input || !*p_input )
    {
        cerr << "error: can't initialize X11 input." << endl;
        return false;
    }

    dump_api_info( cout );
    p_window->title( "collision-box" );
    return true;
}

// compute cycle time (time delta to last frame)
void CollisionBox::get_cycle_time ()
{
    unsigned currCycle = m_lastCycle ^ 1;
    m_timeStamp[currCycle].update();
    m_cycleTime = m_timeStamp[currCycle] - m_timeStamp[m_lastCycle];
    m_lastCycle = currCycle;

    m_cycleTimeMin = std::min( m_cycleTimeMin, m_cycleTime );
    m_cycleTimeMax = std::max( m_cycleTimeMax, m_cycleTime );
    m_cycleTimeAvg = (m_cycleTimeAvg + m_cycleTime) >> 1;
}

// dump statistics (at 1Hz)
void CollisionBox::dump_statistics ()
{
    static const unsigned long ms            = 1000;
    static const unsigned long statsTimerMax = 1000 * ms;
    static       unsigned long triCnt        = 0;

    // print header (only once)
    static bool headerOK = false;
    if ( !headerOK )
    {
        cout << endl;
        cout << "            usec per cycle        ||        cycles per second       ||    triangles per second"                  << endl;
        cout << "            ----------------------||--------------------------------||----------------------------"              << endl;
        cout << "      min   |    max   |    avg   ||    min   |    max   |    avg   ||      min    |      max    |      avg    " << endl;
        cout << "  ----------+----------+----------++----------+----------+----------++-------------+-------------+-------------" << endl;

        triCnt   = p_collider->size() * p_colliderMesh->element_count() / 3;
        headerOK = true;
    }

    m_statsTimer += m_cycleTime;
    if ( statsTimerMax <= m_statsTimer )
    {
        // compute performance indicators
        static const char pAlive[]  = "-\\|/";
        static unsigned   aliveIdx  = 0;
        unsigned long     triPerSec = triCnt * MICROSEC_PER_SEC / m_cycleTime;

        unsigned long fpsMin = 1000 * ms / m_cycleTimeMax;
        unsigned long fpsMax = 1000 * ms / m_cycleTimeMin;
        unsigned long fpsAvg = 1000 * ms / m_cycleTimeAvg;

        unsigned long tpsMin = triCnt * MICROSEC_PER_SEC / m_cycleTimeMin;
        unsigned long tpsMax = triCnt * MICROSEC_PER_SEC / m_cycleTimeMax;
        unsigned long tpsAvg = triCnt * MICROSEC_PER_SEC / m_cycleTimeAvg;

        cout << string( m_statsLen, '\b' );
        m_statsLen = printf( "   %'8lu | %'8lu | %'8lu || %'8lu | %'8lu | %'8lu || %'11lu | %'11lu | %'11lu  %c",
                             m_cycleTimeMin, m_cycleTimeMax, m_cycleTimeAvg,
                             fpsMin, fpsMax, fpsAvg,
                             tpsMax, tpsMin, tpsAvg,
                             pAlive[aliveIdx]
                           );
        cout << std::flush;
        aliveIdx = (aliveIdx + 1) % 4;
        m_statsTimer = 0;
    }
}

// computes unique / hash-like ID for current input state
void CollisionBox::input_state ()
{
    // handle mouse input -> set camera position
    unsigned spaceKey   = p_input->key_down(32);  // rotate cam
    unsigned ctrlKey    = p_input->key_down(227); // translate light-0
    unsigned upKey      = p_input->key_down(82);  // change render mode
    unsigned dnKey      = p_input->key_down(84);  // change normal vector count
    unsigned leKey      = p_input->key_down(81);  // change shading mode (flat, smooth)
    unsigned mouse[]    = { p_input->btn(0), p_input->btn(1), p_input->btn(2) };
    unsigned escKey     = p_input->key_down(27);  // terminate
    m_inputState = (escKey << 7) | (leKey << 6) | (dnKey << 5) | (upKey << 4) | (ctrlKey << 3) | (spaceKey << 2) | (mouse[2] << 1) | (mouse[0] << 0);
}

// pull inputs (at 20Hz == 50ms)
void CollisionBox::handle_inputs ()
{
    m_inputTimer += m_cycleTime;
    static const unsigned long ms            = 1000;
    static const unsigned long inputTimerMax = 50 * ms;
    if ( inputTimerMax <= m_inputTimer )
    {
        m_inputTimer = 0;
        p_input->poll_events();
        input_state();
        switch ( m_handleState | m_inputState )
        {
            case 0x0001: // start translating camera (x/y) => mouse-button-1
            case 0x0002: // start translating camera (z)   => mouse-button-2
                m_cursorOffs  = p_input->cursor();
                m_objectOffs  = p_cam->translate;
                m_handleState = 0xA000;
                break;
            case 0x0005: // start rotating camera (x/y)    => SPACE + mouse-button-1
                m_cursorOffs  = p_input->cursor();
                m_objectOffs  = p_cam->rotate;
                m_handleState = 0xA000;
                break;
            case 0x0009: // start translating light0 (x/y) => CTRL + mouse button 1
            case 0x000A: // start translating light0 (z)   => CTRL + mouse button 2
                m_cursorOffs  = p_input->cursor();
                m_objectOffs  = p_light->location();
                m_handleState = 0xA000;
                break;
            case 0xA001: // translate camera (x/y) => mouse-button-1
            {
                Vertex2D<int> cursorDiff = p_input->cursor() - m_cursorOffs;
                p_cam->translate = m_objectOffs + Vertex3D<float>( -cursorDiff.x, cursorDiff.y, 0 );
                break;
            }
            case 0xA002: // translate camera (z)   => mouse-button-2
            {
                Vertex2D<int> cursorDiff = p_input->cursor() - m_cursorOffs;
                p_cam->translate = m_objectOffs + Vertex3D<float>( 0, 0, -cursorDiff.y );
                break;
            }
            case 0xA005: // rotate camera (x/y)    => SPACE + mouse-button-1
            {
                Vertex2D<int> cursorDiff = p_input->cursor() - m_cursorOffs;
                p_cam->rotate = m_objectOffs + Vertex3D<float>( -cursorDiff.y, -cursorDiff.x, 0 );
                break;
            }
            case 0xA009: // translate light0 (x/y) => CTRL + mouse button 1
            {
                Vertex2D<int> cursorDiff = p_input->cursor() - m_cursorOffs;
                p_lightBulp->translate   = m_objectOffs + Vertex3D<float>( cursorDiff.x, -cursorDiff.y, 0 );
                p_light->location()      = p_lightBulp->translate;
                p_light->render();
                break;
            }
            case 0xA00A: // translate light0 (z)   => CTRL + mouse button 2
            {
                Vertex2D<int> cursorDiff = p_input->cursor() - m_cursorOffs;
                p_lightBulp->translate   = m_objectOffs + Vertex3D<float>( 0, 0, cursorDiff.y );
                p_light->location()      = p_lightBulp->translate;
                p_light->render();
                break;
            }
            case 0x0010: // switch primitive rendering     => UP
            {
                const  unsigned renderModeMax = 2;
                static unsigned renderMode    = 0;
                renderMode = (renderMode + 1) % renderModeMax;

                for ( unsigned idx = 0; idx < p_collider->size(); ++idx )
                {
                    SceneMatrix *pModelMatrix = (SceneMatrix*) p_collider->at( idx );
                    switch ( renderMode )
                    {
                        case 0:
                            pModelMatrix->scene( *p_colliderMesh );
                            break;
                        case 1:
                            pModelMatrix->scene( *p_debugging );
                            break;
                    }
                }
                m_handleState = 0xB000;
                break;
            }
            case 0x0020: // switch normal count
            {
//                static unsigned modeIdx = 1;
//                modeIdx = (modeIdx + 1) % 2;
//                GLint shadeMode[] = { GL_FLAT, GL_SMOOTH };
//                vector<SceneElement*>::iterator it = p_cubes->begin();
//                for ( ; it != p_cubes->end(); ++it )
//                {
//                    SceneModel *pModel = dynamic_cast<SceneModel*>( *it );
//                    if ( pModel )
//                        pModel->shading( shadeMode[modeIdx] );
//                }
//                //cout << "shading mode switch: " << shadeMode[modeIdx] << endl;
//                m_handleState = 0xB000;
                break;
            }
            case 0x0040:  // switch shading (flat, gauroud) => LEFT
            {
                static unsigned modeIdx = 1;
                modeIdx = (modeIdx + 1) % 2;
                glShadeModel( GL_FLAT + modeIdx );
                m_handleState = 0xB000;
                break;
            }
            case 0x0080: // terminate => ESCAPE
                m_run = false;
                break;
            case 0xB010: // idle
            case 0xB020:
                break;
            case 0xB000:
            default: // reset to initial state => no input
                m_handleState = 0;
        }
    }
}

// render next frame
void CollisionBox::render_frame ()
{
    p_window->clear();
    p_cam->render();
    p_window->swap_buffers();

    // compute object collisions
    // compute object forces
    // compute object positions
//        Vertex3D<float> gV( 0, 9.8, 0 );           // gravity acceleration vector
//        vector<SceneObject*>::iterator it = p_scene->begin();
//        for ( ; it != p_scene->end(); ++it )
//        {
//            SceneModel *pModel = (SceneModel*)*it;
//            if ( 1 < pModel->m_scale.x )
//            {
//                pModel->m_location.y += gV.y * 0.0001;
//            //pModel->v.y = 0.1;//pModel->v + gV;
//            }
//        }
}

bool CollisionBox::run ()
{
    p_window->show( true );

    // calibrate time stamps
    m_timeStamp[m_lastCycle].update();
    render_frame();
    m_timeStamp[m_lastCycle ^ 1].update();

    m_run = true;
    while ( m_run )
    {
        render_frame();
        get_cycle_time();
        dump_statistics();
        handle_inputs();
    }

    cout << endl;
    cout << "cam location: "  << p_cam->translate << " / cam rotation: " << p_cam->rotate << endl;
    cout << "bulp location: " << p_lightBulp->translate << endl;
    return true;
}
