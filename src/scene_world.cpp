/*******************************************************************************
 * file   : scene_world.cpp
 * author : Christian Genz
 * email  : genzomatiker@gmail.com
 * info   :
 *******************************************************************************/

#include "scene_world.h"
#include <iostream>
#include "scene_matrix.h"
#include "scene_cube.h"
#include "scene_icosphere.h"
#include "scene_uvsphere.h"
#include "scene_debugger.h"
#include "scene_waves.h"
#include "tracing.h"
#include <unistd.h>
#include <math.h>

using namespace std;


// =============================================================================
// =    utils
// =============================================================================
#define floorWidth      (20.0)

// exit on instantiation errors
#define ERROR_IF( errCond, errMsg )                                            \
    if ( errCond )                                                             \
    {                                                                          \
        cerr << "error: " << errMsg << endl;                                   \
        return false;                                                          \
    }

// =============================================================================
// =    member functions
// =============================================================================
SceneWorld::SceneWorld ()
{
    p_scene        = nullptr;
    p_colliderMesh = nullptr;
    p_collider     = nullptr;
    p_debugging    = nullptr;
    p_light        = nullptr;
    p_lightBulp    = nullptr;
    p_cam          = nullptr;
}

SceneWorld::~SceneWorld ()
{
}

// setup primitives and API configuration for rendering
bool SceneWorld::init ( unsigned sphereCnt, unsigned faceCnt, unsigned w, unsigned h, unsigned meshTyp )
{
    p_scene = new Scene();
    ERROR_IF( !p_scene, "can't initialize benchmark rendering pipeline (scene)." )
    return init_camera() && init_sky_box() &&
           init_colliders( sphereCnt, faceCnt, meshTyp ) &&
           init_debugging() &&
           init_light_bulp();
}

// setup camera angle and location
bool SceneWorld::init_camera ()
{
    p_cam = new SceneMatrix();
    ERROR_IF( !p_cam, "can't initialize camera." )

    p_cam->scene( *p_scene );
    p_cam->mode( 0, 1, 1 );
    p_cam->rotate.set( 25, 0, 0 );
    p_cam->translate.set( 0, -floorWidth * 0.3, -floorWidth * 2 );

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    //glOrtho(-1., 1., -1., 1., 1., 20.);
    glFrustum (-1.0, 1.0, -1.0, 1.0, 1.5, 1000.0);

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    return true;
}

// setup sky box (scale, location, invert culling)
bool SceneWorld::init_sky_box ()
{
    SceneCube *pSkyBoxMesh = new SceneCube();
    ERROR_IF( !pSkyBoxMesh, "can't initialize sky box." )
    p_scene->push_back( pSkyBoxMesh );

    // scale sky box to an absolute width (floorWidth) and translate upwards
    float skyBoxFactor = floorWidth / (2 * fabsf( pSkyBoxMesh->vertex(0).x ));
    for ( unsigned idx = 0; idx < pSkyBoxMesh->element_count(); ++idx )
    {
        pSkyBoxMesh->vertex(idx) = (pSkyBoxMesh->vertex(idx) * skyBoxFactor + Vertex3D<float>( 0, floorWidth / 2 , 0 ));
        pSkyBoxMesh->normal(idx) = pSkyBoxMesh->normal(idx) * -1;
    }

    // invert face indexes so only the inside of the cube can be viewed
    for ( unsigned idx = 0; idx < pSkyBoxMesh->element_count(); idx += 4 )
    {
        unsigned invFace[4] = { pSkyBoxMesh->index(idx+3), pSkyBoxMesh->index(idx+2), pSkyBoxMesh->index(idx+1), pSkyBoxMesh->index(idx) };
        for ( unsigned invIdx = 0; invIdx < 4; ++invIdx )
            pSkyBoxMesh->index( idx + invIdx ) = invFace[invIdx];
    }

    return true;
}

bool SceneWorld::init_colliders ( unsigned sphereCnt, unsigned faceCnt, unsigned meshTyp )
{
    ScenePrimitives *pColliderMesh = nullptr;
    switch ( meshTyp )
    {
        case 0:
            pColliderMesh = new SceneIcoSphere();
            break;
        case 1:
            pColliderMesh = new SceneUvSphere( faceCnt, Vertex3D<float>( 1, 0, 0), Vertex3D<float>( 0, 1, 0 ));
            break;
        case 2:
            pColliderMesh = new SceneCube();
            break;
        case 3:
            pColliderMesh = new SceneWaves( floorWidth/2, floorWidth/2, faceCnt, GL_C4F_N3F_V3F );
            break;
        default:
            cerr << "error: unkown mesh type (" << meshTyp << ")" << endl;
    }

    SceneMatrix *pMatrixList = new SceneMatrix[sphereCnt]();
    p_colliderMesh           = pColliderMesh;
    p_collider               = new Scene();
    ERROR_IF( !pColliderMesh, "can't initialize icosphere mesh." )
    ERROR_IF( !pMatrixList,   "can't initialize colliding matrices." )
    ERROR_IF( !p_collider,    "can't initialize colliders scene container." )

    unsigned xIcoCnt = floorf( sqrtf( sphereCnt ) );
    sphereCnt        = xIcoCnt * xIcoCnt;
    float    cellW   = floorWidth / xIcoCnt;
    float    icoOffs = cellW / 2 - floorWidth / 2;
    float    icoY    =-cellW / 2 + floorWidth * 0.8;

    // evaluate collider mesh radius
    float colliderRadius = 0;
    for ( unsigned idx = 0; idx < pColliderMesh->element_count(); ++idx )
    {
        Vertex3D<float> &v = pColliderMesh->vertex( idx );
        colliderRadius     = max( fabsf(v.x), max( fabsf(v.z), colliderRadius ) );
    }

    // scale collidables mesh, so they all fit into sky box
    float colliderFactor = 0.8 * cellW / (colliderRadius * 2);
    for ( unsigned idx = 0; idx < pColliderMesh->element_count(); ++idx )
        pColliderMesh->vertex( idx ) = pColliderMesh->vertex( idx ) * colliderFactor;

    // place collidables into sky box
    for ( unsigned xCube = 0; xCube < xIcoCnt; ++xCube )
        for ( unsigned zCube = 0; zCube < xIcoCnt; ++zCube )
        {
            unsigned idx = xCube + zCube * xIcoCnt;
            pMatrixList[idx].translate.set( xCube * cellW + icoOffs, icoY, zCube * cellW + icoOffs );
            pMatrixList[idx].scene( *pColliderMesh );
            pMatrixList[idx].mode( 0, 0, 1 );
            p_collider->push_back( &pMatrixList[idx] );
        }

    p_scene->push_back( p_collider );
    return true;
}

bool SceneWorld::init_debugging ()
{
    SceneDebugger *pDebugMesh  = new SceneDebugger( *p_colliderMesh );
    Scene         *pDebugScene = new Scene();
    ERROR_IF( !pDebugMesh,  "can't initialize debugging mesh." )
    ERROR_IF( !pDebugScene, "can't initialize debugging scene container." )

    pDebugScene->push_back( p_colliderMesh );
    pDebugScene->push_back( pDebugMesh );
    p_debugging = pDebugScene;
    return true;
}

// setup light bulb location
bool SceneWorld::init_light_bulp ()
{
    SceneIcoSphere *pLightMesh = new SceneIcoSphere();
    p_lightBulp          = new SceneMatrix();
    p_light              = new SceneLight();
    ERROR_IF( !pLightMesh,  "can't initialize light bulp's mesh." )
    ERROR_IF( !p_lightBulp, "can't initialize light bulp's matix." )
    ERROR_IF( !p_light,     "can't initialize light." )

    // prepare light bulp's mesh
//    pLighBulpMesh->load_sphere( 512, Vertex3f(1,1,0), Vertex3f(1,1,.5) );
//    pLighBulpMesh->scale().set( floorWidth/10, floorWidth/10, floorWidth/10 );

    // prepare light bulp's matrix
    p_lightBulp->scene( *pLightMesh );
    p_lightBulp->translate.set( floorWidth*-1.25, floorWidth*1.25, floorWidth*1.0 );
    p_lightBulp->mode( 0, 0, 1 );

    // add light bulp to scene
    p_scene->push_back( p_lightBulp );

    // setup light location
    p_light->location() = p_lightBulp->translate;
    p_light->init();

    return true;
}
