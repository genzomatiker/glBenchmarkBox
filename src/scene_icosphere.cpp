/*******************************************************************************
* file   : scene_icosphere.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "scene_icosphere.h"
#include <GL/gl.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include "scene_cube.h"
#include "tracing.h" // <---- only for trace/debug purposes -> remove

// =============================================================================
// =               cubes geometric details                                     =
// =============================================================================
namespace icosphere
{
    #define Tl                    ( (  1.0 + sqrtf( 5.0 ) ) / 2.0 )
    Vertex3D<float> pVertices[] = { { -1, Tl,  0 },     //  [0]
                                    {  1, Tl,  0 },     //  [1]
                                    { -1,-Tl,  0 },     //  [2]
                                    {  1,-Tl,  0 },     //  [3]
                                    {  0, -1, Tl },     //  [4]
                                    {  0,  1, Tl },     //  [5]
                                    {  0, -1,-Tl },     //  [6]
                                    {  0,  1,-Tl },     //  [7]
                                    { Tl,  0, -1 },     //  [8]
                                    { Tl,  0,  1 },     //  [9]
                                    {-Tl,  0, -1 },     // [10]
                                    {-Tl,  0,  1 } };   // [11]

    unsigned        pIndexes[] =    {  0, 11,  5,       //  [0]
                                       0,  5,  1,       //  [1]
                                       0,  1,  7,       //  [2]
                                       0,  7, 10,       //  [3]
                                       0, 10, 11,       //  [4]
                                       1,  5,  9,       //  [5]
                                       5, 11,  4,       //  [6]
                                      11, 10,  2,       //  [7]
                                      10,  7,  6,       //  [8]
                                       7,  1,  8,       //  [9]
                                       3,  9,  4,       // [10]
                                       3,  4,  2,       // [11]
                                       3,  2,  6,       // [12]
                                       3,  6,  8,       // [13]
                                       3,  8,  9,       // [14]
                                       4,  9,  5,       // [15]
                                       2,  4, 11,       // [16]
                                       6,  2, 10,       // [17]
                                       8,  6,  7,       // [18]
                                       9,  8,  1 };     // [19]

    Vertex3D<float> pColors[] =   { {  1,  0,  0 },     //  [0] left  up     front
                                    { .7, .4,  0 },     //  [1] right up     front
                                    { .4, .7,  0 },     //  [2] right bottom front
                                    { .7, .4,  0 },     //  [3] left  bottom front
                                    { .7, .4,  0 },     //  [4] right up     back
                                    { .4, .7,  0 },     //  [5] left  up     back
                                    { .1,  1,  0 },     //  [6] right bottom back
                                    { .4, .7,  0 } };   //  [7] left  bottom back

    unsigned        faceEdgeCnt = 3;
}


using namespace icosphere;
using namespace std;

// =============================================================================
// =               utils                                                       =
// =============================================================================
Vertex3D<float> plane_normal_3 ( unsigned planeIdx )
{
    unsigned        nI    = planeIdx * 3;
    Vertex3D<float> tV[3] = { pVertices[pIndexes[nI]], pVertices[pIndexes[nI+1]], pVertices[pIndexes[nI+2]] };
    Vertex3D<float> lightVector = (tV[1]-tV[0]) % (tV[2]-tV[0]);
    return lightVector.normalize();
}

// =============================================================================
// =               class members                                               =
// =============================================================================
SceneIcoSphere::SceneIcoSphere() : ScenePrimitives()
{
    init();
}

// assign vertices, colors, normals and indexes
bool SceneIcoSphere::init ()
{
    // instantiate members
    unsigned elemCnt = sizeof( pIndexes ) / sizeof( unsigned );
    bool allocOK     = ScenePrimitives::init( elemCnt, GL_TRIANGLES, GL_C4F_N3F_V3F );
    if ( allocOK )
        for ( unsigned idx = 0; idx < m_indexCnt; ++idx )
        {
            color( idx )[0] = pVertices[ pIndexes[idx] ].x;
            color( idx )[1] = pVertices[ pIndexes[idx] ].y;
            color( idx )[2] = pVertices[ pIndexes[idx] ].z;
            color( idx )[3] = 1;
            normal( idx )   = plane_normal_3( idx / faceEdgeCnt );
            vertex( idx )   = pVertices[ pIndexes[idx] ];
            index( idx )    = idx;
        }

    return allocOK;
}
