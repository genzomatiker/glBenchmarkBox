/*******************************************************************************
* file   : scene_light.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "scene_light.h"
#include <GL/gl.h>

#define INVALID_LIGHT_ID       (-1)


SceneLight::SceneLight ()
{
    // flag light id invalid
    m_id         = INVALID_LIGHT_ID;
    m_color.set( 3, 3, 3 );
    m_brightness = 1;
}

SceneLight::~SceneLight ()
{
    if ( valid_id() )
        glDisable( GL_LIGHT0 + m_id );
}

void SceneLight::init ()
{
    init( GL_AMBIENT, 1 );  //  GL_DIFFUSE GL_SPECULAR
}

void SceneLight::init ( int lightType, float lightValue )
{
    if ( !valid_id() )
    {
        // max. supported openGL light count
        GLint maxLights;
        glGetIntegerv( GL_MAX_LIGHTS, &maxLights );
        glLightModeli( GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE );

        // find next free light ID (if none given)
        for ( int id = 0; (-1 == m_id) && (id < maxLights); ++id )
            m_id = glIsEnabled( GL_LIGHT0 + id ) ? m_id : id;

        // found a free light index?
        if ( valid_id() )
        {
            // describe the light
            // try this from red book
            //GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
            //GLfloat mat_shininess[] = { 50.0 };
            //glMaterialfv( GL_FRONT, GL_SPECULAR, mat_specular);
            //glMaterialfv( GL_FRONT, GL_SHININESS, mat_shininess);
            //glLightfv( GL_LIGHT0 + m_id, GL_COLOR,    &m_color[0] );

            Vertex3D<float> color      = m_color * m_brightness;
            GLfloat         lightCol[] = { color.x, color.y, color.z, 1.0 };

            //glLightfv( GL_LIGHT0 + m_id, GL_SPECULAR, lightCol );
            glLightfv( GL_LIGHT0 + m_id, GL_DIFFUSE, lightCol );

            render();

            // let there be light ...
            glEnable( GL_LIGHTING );
            glEnable( GL_LIGHT0 + m_id );
            glEnable( GL_COLOR_MATERIAL );
        }
    }
}

void SceneLight::render () const
{
    float lightPos[4] = { m_location.x, m_location.y, m_location.z, 0 };
    glLightfv( GL_LIGHT0 + m_id, GL_POSITION, lightPos );
}

bool SceneLight::valid_id () const
{
    return INVALID_LIGHT_ID != m_id;
}

Vertex3D<float>& SceneLight::rotation ()
{
    return m_rotation;
}

Vertex3D<float>& SceneLight::location ()
{
    return m_location;
}
