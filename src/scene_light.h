/*******************************************************************************
* file   : scene_light.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef scene_light_H
#define scene_light_H

#include "scene_element.h"
#include "vertex_3d.h"


struct SceneLight : public SceneElement
{
                            SceneLight ();
                           ~SceneLight ();
    virtual void            init ();
    virtual void            init ( int lightType, float lightValue );
    virtual void            render () const;
    bool                    valid_id () const;

public: // stolen from SceneObject
    Vertex3D<float>&        rotation ();
    Vertex3D<float>&        location ();

protected:
    int                             m_id;
    Vertex3D<float>                 m_color;
    float                           m_brightness;
protected: // stolen from SceneObject
    Vertex3D<float>                 m_rotation;
    Vertex3D<float>                 m_location;
};

#endif   // scene_light_H
