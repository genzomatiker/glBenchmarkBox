/*******************************************************************************
* file   : scene_uvsphere.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef scene_uvsphere_H
#define scene_uvsphere_H

#include "scene_primitives.h"


struct SceneUvSphere : public ScenePrimitives
{
                            SceneUvSphere ( unsigned vertexCnt, Vertex3D<float> tCol, Vertex3D<float> bCol );
    bool                    init ();

protected:
    Vertex3D<float>                 m_colRange[2];
};

#endif   // scene_uvsphere_H
