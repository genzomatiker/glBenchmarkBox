/*******************************************************************************
 * file   : collision_box.h
 * author : Christian Genz
 * email  : genzomatiker@gmail.com
 * info   :
 *******************************************************************************/

#ifndef collision_box_H
#define collision_box_H


#include "glx_display.h"
#include "glx_window.h"
#include "glx_input.h"
#include "scene_world.h"
#include "time_stamp.h"
#include "vertex_3d.h"
#include <iostream>


class CollisionBox : public SceneWorld
{
public:
                    CollisionBox ();
                   ~CollisionBox ();
    void            dump_api_info ( std::ostream &dst );
    bool            init ( unsigned sphereCnt, unsigned faceCnt, unsigned w, unsigned h, unsigned meshTyp = 0 );
    bool            init_window ( unsigned w, unsigned h );
    void            get_cycle_time ();
    void            dump_statistics ();
    void            handle_inputs ();
    void            input_state ();
    void            render_frame ();
    bool            run ();

protected:
    bool                            m_run;
    GlxDisplay*                     p_display;
    GlxWindow*                      p_window;
    // -------------------------------------------------------------------------
    TimeStamp                       m_timeStamp[2];
    unsigned                        m_lastCycle;
    unsigned long                   m_cycleTime;        // last cycle in usec
    unsigned long                   m_cycleTimeMin;
    unsigned long                   m_cycleTimeMax;
    unsigned long                   m_cycleTimeAvg;
    // -------------------------------------------------------------------------
    GlxInput*                       p_input;
    unsigned long                   m_inputTimer;
    unsigned                        m_inputState;
    unsigned                        m_handleState;
    Vertex2D<int>                   m_cursorOffs;
    Vertex3D<float>                 m_objectOffs;
    // -------------------------------------------------------------------------
    unsigned long                   m_statsTimer;
    unsigned                        m_statsLen;
};

#endif  // collision_box_H
