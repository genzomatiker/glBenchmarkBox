/*******************************************************************************
* file   : scene_primitives.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef scene_primitives_H
#define scene_primitives_H

#include "GL/gl.h"
#include "scene_element.h"
#include "vertex_3d.h"


// NOTE: make this class be render-able only from derived classes!
// => protected CTOR / protected render()
struct ScenePrimitives : public SceneElement
{
                            ScenePrimitives ();
    bool                    init ( unsigned elemCnt, GLenum mode, GLenum format );
    // -------------------------------------------------------------------------
    GLenum                  mode () const;
    GLenum                  format () const;
    unsigned                element_count () const;
    // -------------------------------------------------------------------------
    unsigned&               index ( unsigned idx );
    Vertex3D<float>&        color ( unsigned idx );
    Vertex3D<float>&        texel ( unsigned idx );
    Vertex3D<float>&        normal ( unsigned idx );
    Vertex3D<float>&        vertex ( unsigned idx );
    // -------------------------------------------------------------------------
    virtual void            render () const;
    void                    register_array ();
    void                    render_glBeginEnd ();
    void                    render_elements ();
    void                    render_array ();
    void                    draw_vbo ();
    void                    draw_dl ();

protected:
    GLvoid*                         p_mesh;         // intertwines vertices, normals, texels, colors
    GLenum                          m_meshType;     // primitive type (GL_TRIANGLE, GL_LINE, ...)
    GLenum                          m_meshFormat;   // specifies active array types within mesh
    GLsizei                         m_meshStride;   // bytes between consecutive vertexes in mesh
    unsigned*                       p_indexes;      // indexes of primitives in mesh
    GLsizei                         m_indexCnt;     // number of indexes
};

#endif   // scene_primitives_H
