/*******************************************************************************
* file   : glx_xvisual_info.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef glx_xvisual_info_H
#define glx_xvisual_info_H

#include "error_counter.h"
#include "glx_display.h"
#include "glx_attr_list.h"
#include <GL/glx.h>


// Simplifies extraction of an XVisualInfo from a given GLX frame buffer config.
// Without this object the user has to check which GLX API version is installed.
class GlxXVisualInfo : public ErrorCounter
{
public:
                               ~GlxXVisualInfo ();
                                GlxXVisualInfo ( GlxDisplay &dspl, GLXFBConfig &conf );
                                GlxXVisualInfo ( GlxDisplay &dspl, int scr, GlxAttrList &attrList );
    XVisualInfo*                get_info ();

protected:
    unsigned                        m_vErrCnt;      // error counter
    GlxDisplay&                     m_display;
    XVisualInfo*                    p_visInfo;
};

#endif  // glx_xvisual_info_H
