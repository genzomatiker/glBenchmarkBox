/*******************************************************************************
* file   : glx_input.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef glx_input_H
#define glx_input_H

#include "error_counter.h"
#include "glx_window.h"
#include "vertex_2d.h"


#define DEFAULT_EVENTS              (ExposureMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask)


// Class instances can poll and retain the state of all registered inputs. They
// simplify display, keyboard, mouse or other input management of the Xlib API.
class GlxInput : public ErrorCounter
{
public:
                            GlxInput ( GlxWindow &win, long events = DEFAULT_EVENTS );
    unsigned                poll_events ();
                           ~GlxInput ();
    GlxInput&               read_event ();
    bool                    key_down ( unsigned key ) const;
    bool                    btn ( unsigned btnNum ) const;
    const Vertex2D<int>&    cursor () const;

protected:
    unsigned                        m_xErrCnt;          // error counter
    GlxWindow&                      m_window;           // X11 event window
    unsigned                        m_keyEventCnt;      // keyboard event counter
    char                            m_key[256];         // keyboard key states
    unsigned                        m_btnEventCnt;      // mouse button event counter
    unsigned                        m_btn[5];           // mouse button coordinates
    unsigned                        m_cursorEventCnt;   // mouse pointer event counter
    Vertex2D<int>                   m_cursor;           // mouse pointer absolute coordinates
};

#endif  // glx_input_H
