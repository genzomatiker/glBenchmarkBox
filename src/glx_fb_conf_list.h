/*******************************************************************************
* file   : glx_fb_conf_list.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef glx_fb_conf_list_H
#define glx_fb_conf_list_H

#include "error_counter.h"
#include "glx_display.h"
#include "glx_attr_list.h"
#include <GL/glx.h>
#include <vector>


// Simplifies the identification and filtering of GLS frame buffer configurations.
// NOTE: GLXFBConfig is a pointer => don't worry about entity addresses
class GlxFbConfList : public ErrorCounter, public std::vector<GLXFBConfig>
{
public:
                                GlxFbConfList ( GlxDisplay &dspl, int scr, GlxAttrList &attrList );
    GLXFBConfig                 find_by_attr_max ( int attrCode );
    GLXFBConfig                 find_by_attr_min ( int attrCode );

protected:
    unsigned                        m_cfgErrCnt;      // error counter
    GlxDisplay&                     m_display;
    int                             m_screen;
};

#endif  // glx_fb_conf_list_H
