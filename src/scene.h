/*******************************************************************************
* file   : scene.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef scene_H
#define scene_H

#include <vector>
#include "scene_element.h"


struct Scene : public std::vector<SceneElement*>, public SceneElement
{
                            Scene ();
    virtual void            init ();
    virtual void            render () const;
};

#endif   // scene_H
