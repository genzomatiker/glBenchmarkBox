/*******************************************************************************
* file   : tracing.cpp.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "tracing.h"
#include <iostream>
#include <iomanip>


template<typename T>
std::ostream& operator << ( std::ostream &dst, const Vertex3D<T> &vert )
{
    dst << "( " << std::fixed << std::setprecision(2) << std::setw(5) << vert.x << ", " << std::setw(6) << vert.y << ", " << std::setw(6) << vert.z << " )";
    return dst;
}

template<typename T>
std::ostream& operator << ( std::ostream &dst, const std::vector<Vertex3D<T> > &vertList )
{
    for ( unsigned idx = 0; idx < vertList.size(); ++idx )
        dst << "vertex[" << idx << "] = " << vertList[idx] << std::endl;
    return dst;
}
