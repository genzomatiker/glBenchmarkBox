/*******************************************************************************
* file   : scene_element.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef scene_element_H
#define scene_element_H


// Base class for all elements of a scene that can be rendered and initialized.
struct SceneElement
{
    virtual void            render () const = 0;
};

#endif   // scene_element_H
