/*******************************************************************************
* file   : scene_icosphere.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef scene_icosphere_H
#define scene_icosphere_H

#include "scene_primitives.h"


struct SceneIcoSphere : public ScenePrimitives
{
                            SceneIcoSphere ();
    bool                    init ();
};

#endif   // scene_icosphere_H
