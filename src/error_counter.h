/*******************************************************************************
* file   : error_counter.h
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#ifndef error_counter_H
#define error_counter_H


class ErrorCounter
{
public:
                        ErrorCounter ( unsigned &errCnt );
                        operator bool () const;
    unsigned&           errors();

protected:
    unsigned&                       m_errorCnt;
};

#endif  // error_counter_H
