/*******************************************************************************
* file   : glx_fb_conf_list.cpp
* author : Christian Genz
* email  : genzomatiker@gmail.com
*******************************************************************************/

#include "glx_fb_conf_list.h"
#include <GL/glx.h>
#include <iostream>
#include <climits>

using namespace std;


GlxFbConfList::GlxFbConfList ( GlxDisplay &dspl, int scr, GlxAttrList &attrList ) : ErrorCounter( m_cfgErrCnt ),
                                                                                    m_display( dspl ),
                                                                                    m_screen( scr )
{
    // find all FB attr configs that match the required attributes
    const int *pAttrList = attrList.create_int_list();
    if ( pAttrList )
    {
        int          fbConfCnt   = 0;
        GLXFBConfig *pFbConfList = glXChooseFBConfig( m_display.display(), m_screen, pAttrList, &fbConfCnt );
        if ( pFbConfList )
        {
            // copy FB config pointers to object (YEP: index overflow is allowed <= not used by assign)
            assign( pFbConfList, &pFbConfList[fbConfCnt] );

            // release config list memory, allocated by glXChooseFBConfig()
            XFree( pFbConfList );
        }
        else
        {
            cerr << "error: failed to query GLX frame buffer configurations!" << endl;
            ++errors();
        }

        delete []pAttrList;
    }
}

GLXFBConfig GlxFbConfList::find_by_attr_max ( int attrCode )
{
    GLXFBConfig pRval      = nullptr;
    int         attrValMax = INT_MIN;

    GlxFbConfList::iterator cfgIt = begin();
    for ( ; cfgIt != end(); cfgIt )
    {
        int  attrVal = 0;
        bool attrOk = Success == glXGetFBConfigAttrib( m_display.display(), *cfgIt, attrCode, &attrVal );
        if ( attrOk && (!pRval || attrValMax < attrVal) )
        {
            attrValMax = attrVal;
            pRval      = *cfgIt;
        }
    }

    return pRval;
}

GLXFBConfig GlxFbConfList::find_by_attr_min ( int attrCode )
{
    GLXFBConfig pRval      = nullptr;
    int         attrValMin = INT_MAX;

    GlxFbConfList::iterator cfgIt = begin();
    for ( ; cfgIt != end(); cfgIt )
    {
        int  attrVal = 0;
        bool attrOk = Success == glXGetFBConfigAttrib( m_display.display(), *cfgIt, attrCode, &attrVal );
        if ( attrOk && (!pRval || attrVal < attrValMin) )
        {
            attrValMin = attrVal;
            pRval      = *cfgIt;
        }
    }

    return pRval;
}
