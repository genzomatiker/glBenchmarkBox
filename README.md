################################################################################
# Copyright 2015 Christian Genz
################################################################################


0. Content:
----------
  1. About
  2. How to build
  3. How to use
  4. Dependencies
  5. License


1. About:
--------
  a. Abstract:
  ...........
    The current project is a benchmark environment. It creates primitive
    geometries (cubes, spheres, etc.) that act dynamically within an invisible
    bounding box.


2. How to build:
---------------
  - make -C src


3. How to use:
-------------
  - just start the binary without arguments or with --help to show help message.


4. Dependencies:
---------------
  - GNU gcc (version 4.8.2 or higher)
  - GNU make (version 3.81 or higher)
  - GLIBC (version 2.19 or higher)
  - openGL (version 2.x or higher)
  - arglist lib. (version 1.0 or higher)


5. License:
----------
  All documents of the project at hand are distributed under GPLv3.
